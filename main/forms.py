from django import forms
from .models import MainPageInfo
from django.utils.translation import gettext_lazy as _


class MainPageInfoForm(forms.ModelForm):

    class Meta:
        model = MainPageInfo
        fields = '__all__'

        widgets = {
            'organization': forms.Select(attrs={
                "style": "height:50px",
                # "class": "my-3",
                # "style": "height:50px"
            }),
            'inter_org_tel': forms.TextInput(attrs={
                "style": "height:50px",

            }),
            'joined_date': forms.DateInput(
                format='%d/%m/%Y',
                attrs={
                    'style': "height:50px",
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'img': forms.FileInput(attrs={

            }),
            'description_en': forms.Textarea(attrs={
                'style': "height:8rem"
            }),
            'description_uz': forms.Textarea(attrs={
                'style': "height:8rem"
            }),
            'description_ru': forms.Textarea(attrs={
                'style': "height:8rem"
            }),
            'inter_org_address_en': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'inter_org_address_ru': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'inter_org_address_uz': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'meeting_date': forms.DateInput(
                format='%d/%m/%Y',
                attrs={
                    'style': "height:50px",
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            ),
            'meeting_text_uz': forms.Textarea(attrs={
                'style': "height:8rem"
            }),
            'meeting_text_ru': forms.Textarea(attrs={
                'style': "height:8rem"
            }),
            'meeting_text_en': forms.Textarea(attrs={
                'style': "height:8rem"
            }),

        }

        labels = {
            'organization': _('Xalqaro tashkilot'),
            'img': _("Tashkilotning O'zbekistondagi faoliyatini o'zida aks etiruvchi fotolavha"),
            'joined_date': _("Xalqaro tashkilotga a'zo bo'lgan sanasi"),
            'inter_org_tel': _("Telefon raqam"),
            'inter_org_address_uz': _("Tashkilot filiali manzili (uz)"),
            'inter_org_address_ru': _("Tashkilot filiali manzili (ru)"),
            'inter_org_address_en': _("Tashkilot filiali manzili (ўз)"),
            'description_en': _("Faoliyati haqida (ўз)"),
            'description_uz': _("Faoliyati haqida (uz)"),
            'description_ru': _("Faoliyati haqida (ru)"),
            'meeting_date': _("Tadbir sanasi"),
            'meeting_text_uz': _("Tadbir haqida (uz)"),
            'meeting_text_ru': _("Tadbir haqida (ru)"),
            'meeting_text_en': _("Tadbir haqida (ўз)"),
            'inter_organization': _("Xalqaro tashkilot")
        }