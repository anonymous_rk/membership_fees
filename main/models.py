from django.db import models
from organizations import models as org_models
from django.utils.translation import gettext_lazy as _
from .validators import file_validator, phone_nb_validator
from django.core.validators import MaxLengthValidator, MinLengthValidator
from organizations.models import InterOrganization, LinkedAuthInterOrgs


class MainPageInfo(models.Model):
    organization = models.ForeignKey(LinkedAuthInterOrgs, on_delete=models.CASCADE, default=1)
    joined_date = models.DateField()
    description_uz = models.TextField(max_length=2000)
    description_ru = models.TextField(max_length=2000)
    description_en = models.TextField(max_length=2000)
    img = models.ImageField(upload_to='main/org_images/', validators=[file_validator])
    inter_org_address_uz = models.CharField(max_length=300, default=_('Mavjud emas'))
    inter_org_address_ru = models.CharField(max_length=300, default=_('Mavjud emas'))
    inter_org_address_en = models.CharField(max_length=300, default=_('Mavjud emas'))
    inter_org_tel = models.CharField(max_length=13, validators=[MaxLengthValidator(13), MinLengthValidator(9), phone_nb_validator])
    meeting_date = models.DateField(null=True, blank=True)
    meeting_text_uz = models.TextField(null=True, blank=True, max_length=1000)
    meeting_text_ru = models.TextField(null=True, blank=True, max_length=1000)
    meeting_text_en = models.TextField(null=True, blank=True, max_length=1000)
