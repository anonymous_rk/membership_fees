from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _
import os


def file_validator(file):
    ext = os.path.splitext(file.name)[-1]
    if not ext.lower() in ['.jpg', '.jpeg', '.png']:
        raise ValidationError("File formati quyidagilardan biri bo'lishi lozim: '.jpg', '.jpeg', '.png")
    return file


def phone_nb_validator(phone_number):
    if not phone_number.startswith("+998") and len(phone_number) == 13 or "+" in phone_number and not phone_number[1::].isnumeric():
        raise ValidationError(_("Iltimos, telefon raqamni to'g'ri kiriting. Namuna: +998999999999"))
    if len(phone_number) == 9 and not phone_number.isnumeric():
        raise ValidationError(_("Iltimos, telefon raqamni to'g'ri kiriting. Namuna: +998999999999"))

    return phone_number