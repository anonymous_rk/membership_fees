from django.shortcuts import render, get_object_or_404
import requests, json
from django.utils.timezone import now
from django.http import JsonResponse
# from bs4 import BeautifulSoup
from django.views.generic import ListView, CreateView, UpdateView, DetailView
from .models import MainPageInfo
from .forms import MainPageInfoForm
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from organizations.models import LinkedAuthInterOrgs, InterOrganization
from django.contrib.auth.decorators import permission_required, login_required

CURRENT_DATE_TIME = now().strftime('%Y-%m-%d')

PROXIES = {
    "http": "http://192.168.254.242:3128",
    "https": "http://192.168.254.242:3128",
}


# def update_toke():
#     url = "https://cabinet.uzrvb.uz/login.jsp"
#
#     payload = "{\n        \"user_name\":\"anonymous_rk\",\n   " \
#               " \"password\":\"rPtMASrVai4pNVy\",\n   }"
#               # " \"client_id\":\"123456abcd\",\n}"
#
#     # headers = {
#     #     'Content-Type': 'application/json'
#     # }
#
#     request1 = requests.request('POST', url, data=payload)
#     print(request1.text)
#
#
# update_toke()

def get_token():
    url = "https://cabinet.uzrvb.uz/api/get_access_token"

    payload = "{\n        \"user_name\":\"anonymous_rk\",\n   " \
              " \"password\":\"rPtMASrVai4pNVy\",\n   " \
              " \"client_id\":\"123456abcd\",\n   " \
              " \"client_secret\":\"2dcddbc3b2dd6031fb789e71a50a25eaf45ca9db8824c21ee80b9c6c184f4989\"\n}"
    headers = {
        'Content-Type': 'application/json'
    }

    # print(payload)
    birja_data = requests.request("POST", url, headers=headers, data=payload, proxies=PROXIES)

    token_data = json.loads(birja_data.text)["ResponseParams"]
    token = f"{token_data['token_type']} {token_data['access_token']}"
    url1 = "https://cabinet.uzrvb.uz/api/get_security_price"
    #
    result = []
    for i in ["USD", "EUR"]:
        payload1 = "{\n  \"product_code\": \"" + i + "/SUM_IBTS\",\n  \"date\": \"" + CURRENT_DATE_TIME + "\"\n}\n\n\n"
        headers1 = {
            'Authorization': token,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        request_1 = requests.request('POST', url1, headers=headers1, data=payload1, proxies=PROXIES)
        currency_data = json.loads(request_1.text)["ResponseParams"]
        currency_data["OfficialPrice"] = currency_data["OfficialPrice"][:8]
        if '.' not in currency_data['Diff']:
            currency_data["Diff"] = int(currency_data["Diff"])
        currency_data["Diff"] = float(currency_data["Diff"])
        result.append(currency_data)

    # print(result)
    return result


def cb(request):
    cb_data = requests.get('http://cbu.uz/ru/arkhiv-kursov-valyut/json/?base=USD', proxies=PROXIES)
    cb_json_data = json.loads(cb_data.text)
    central_bank = []
    for json_data in cb_json_data:
        if '.' not in json_data["Diff"]:
            json_data["Diff"] = int(json_data["Diff"])
        json_data["Diff"] = float(json_data["Diff"])

        if json_data["Ccy"] == 'USD' or json_data["Ccy"] == "CAD" or json_data["Ccy"] == "RUB" or json_data[
            "Ccy"] == "CHF" or json_data["Ccy"] == "EUR":
            central_bank.append(json_data)

    # print(central_bank)
    return central_bank


def valyutakurs(request):
    usd = get_token()[0]
    eur = get_token()[1]
    cb1 = cb(request)

    context = {
        'today': CURRENT_DATE_TIME
    }
    if usd:
        context['birja_usd'] = usd
    if eur:
        context['birja_eur'] = eur
    if cb1:
        context['central_bank'] = cb1

    data = MainPageInfo.objects.filter(joined_date__month=now().month)
    diffs = []
    day = now().day

    for datum in data:
        joined_day = datum.joined_date.day
        diff = day - joined_day
        if diff < 0:
            diff = -diff

        diffs.append({"diff": diff, "datum": datum})

    info = 1
    if len(diffs) != 0:
        info = min(diffs, key=lambda x: x["diff"])['datum']
    context['info'] = info
    return render(request, 'main/initial_page.html', context)


def for_calculator(request):
    data_type = request.GET.get('type')
    currency_name = request.GET.get('name')
    cb_data = cb(request)

    context = {}
    if data_type == 'birja':
        if currency_name == 'USD':
            context = {
                'price': get_token()[0]['OfficialPrice']
            }
        elif currency_name == 'EUR':
            context = {
                'price': get_token()[1]['OfficialPrice']
            }
    elif data_type == 'cb':
        cb_price = cb(request)
        if currency_name == 'USD':
            context = {
                'price': cb_data[0]['Rate']
            }
        elif currency_name == 'EUR':
            context = {
                'price': cb_data[1]['Rate']
            }
        elif currency_name == 'RUB':
            context = {
                'price': cb_data[2]['Rate']
            }
        elif currency_name == 'CAD':
            context = {
                'price': cb_data[3]['Rate']
            }
        elif currency_name == 'CHF':
            context = {
                'price': cb_data[4]['Rate']
            }

    return JsonResponse(context)


class MainInfoListView(ListView, PermissionRequiredMixin):
    """Main info Listview"""
    login_url = '/users/login/'
    queryset = MainPageInfo.objects.order_by('-id')
    # model = MainPageInfo.objects.all()
    template_name = 'main/main_info.html'
    permission_required = 'main.view_mainpageinfo'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MainInfoListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.queryset
        linked_org = LinkedAuthInterOrgs.objects.filter(auth_organization=self.request.user.auth_organization,
                                                        deleted=False).values_list('inter_organization_id', flat=True)

        context = {
            'object_list': query,
            'inter_org_perms': linked_org
        }
        return render(request, self.template_name, context)


class MainInfoDetail(DetailView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = MainPageInfo
    template_name = 'main/main_info_detail.html'
    permission_required = 'main.view_mainpageinfo'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MainInfoDetail, self).dispatch(request, *args, **kwargs)

    def get(self, request, id, *args, **kwargs):
        query = self.model.objects.get(id=id)
        linked_org = LinkedAuthInterOrgs.objects.filter(auth_organization=self.request.user.auth_organization,
                                                        deleted=False).values_list('inter_organization_id', flat=True)
        context = {
            'o': query,
            'inter_org_perms': linked_org
        }
        return render(request, self.template_name, context)


class CreateMainInfo(CreateView, PermissionRequiredMixin):
    """ Main info Create part """
    login_url = '/users/login/'
    form_class = MainPageInfoForm
    model = MainPageInfo
    template_name = 'main/create_main_info.html'
    permission_required = 'main.add_mainpageinfo'
    raise_exception = True
    permission_denied_message = "You are not permitted to do this"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(CreateMainInfo, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        query = self.model
        form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False,
                                                                                  auth_organization_id=request.user.auth_organization.id)

        if request.user.role == 'Admin':
            form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)

        context = {
            'form': form,
            'object': query
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST, files=request.FILES)
        form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False,
                                                                                  auth_organization_id=request.user.auth_organization.id)
        if request.user.role == 'Admin':
            form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)

        if form.is_valid():
            data = form.save(commit=False)
            data.save()
            return redirect('main:main_info')

        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class UpdateMainInfo(UpdateView, PermissionRequiredMixin):
    """ Main info update part"""
    login_url = '/users/login/'
    model = MainPageInfo
    form_class = MainPageInfoForm
    template_name = 'main/update_main_info.html'
    permission_required = 'main.change_mainpageinfo'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(UpdateMainInfo, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(self.model, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = self.form_class(instance=query)
            form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.filter(
                auth_organization_id=request.user.auth_organization.id)
            if request.user.role == 'Admin':
                form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.all()

            context = {'form': form, 'object': query}
            return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = self.form_class(data=request.POST, files=request.FILES, instance=query)
            form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.filter(
                auth_organization_id=request.user.auth_organization.id)
            if request.user.role == 'Admin':
                form.fields['organization'].queryset = LinkedAuthInterOrgs.objects.all()

            if form.is_valid():
                data = form.save(commit=False)
                data.save()
                return redirect('main:main_info')

        return self.get(request, *args, **kwargs)


@permission_required('main.delete_mainpageinfo', raise_exception=True)
@login_required(login_url='users:login')
def main_info_delete(request, id):
    """ Main info delete part"""

    organ = get_object_or_404(MainPageInfo, id=id)
    if request.method == 'POST':
        organ.delete()
        return redirect('main:main_info')
    context = {'o': organ, 'id': id}

    return render(request, 'main/delete_main_info.html', context)
