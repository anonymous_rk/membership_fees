from django.shortcuts import render


def error_403(request, exception):
    # context = RequestContext(request)
    # response = render(request, '500.html', context)
    # response.status_code = 403
    return render(request, 'errors/403.html')

def error_404(request, exception):
    """ some context """
    return render(request, 'errors/404.html')

def error_500(request):
    """ some context """
    return render(request, 'errors/500.html')

def error_400(request):
    """ some context """
    return render(request, 'errors/400.html')