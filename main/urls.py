from django.urls import path, include
from .views import valyutakurs, for_calculator, MainInfoListView, CreateMainInfo, UpdateMainInfo, main_info_delete, MainInfoDetail

urlpatterns = [
    # path('', home, name='home'),
    # path('', ValyutaView.as_view(), name='home')
    path('', valyutakurs, name='home'),
    path('calculator/', for_calculator, name='calculator'),
    path('main_info/', MainInfoListView.as_view(), name='main_info'),
    path('create_info/', CreateMainInfo.as_view(), name='create_info'),
    path('update_info/<int:id>/', UpdateMainInfo.as_view(), name='update_info'),
    path('delete_info/<int:id>/', main_info_delete, name='delete_info'),
    path('detail_info/<int:id>/', MainInfoDetail.as_view(), name='detail_info')
    # path('update_info/<int:id>/', UpdateMainInfo.as_view(), name='update_info')
]
