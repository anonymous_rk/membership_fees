��          |      �             !  
   2  G   =  9   �     �  h   �     4     M  /   g  5   �  1   �  ;  �     ;     R  ;   a  <   �     �  W   �     >     S  *   k  0   �  *   �               
   	                                         Bank service fee Currencies File on confirmation of  sent invoice  from international organization. File on confirmation of paid fee from local organization. Fiscal year Information on confirmation of receipt of the amount of membership fee to the international organization Membership fee condition Membership fees condition The amount of invoice sent for the current year The amount of membership fee paid in the current year The forecast amount of the membership fee payment Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Bank komissiya xizmati Pul birliklari Xalqaro tashkilot tomonidan jo'natilgan invoysni tasdiqlash Joriy yil uchun to'langan a'zoli badali summasini tasdiqlash Moliya yili A'zolik badali to'langanligi haqidagi xalqaro tashkilot tomonidan tasdiqlovchi ma'lumot Badal to'lovi holati Badal to'lovlari holati Joriy yil uchun jo'natilgan invoys summasi Joriy yil uchun to'langan a'zolik badali summasi A'zolik badali to'lovining prognoz summasi 