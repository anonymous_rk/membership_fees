from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from .models import CustomUser
from .forms import PasswordChangeForms, CustomUserCreationForm, ProfileImageForm, UserLoginForm
from django.contrib.auth import logout, login
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView
from organizations.models import AuthOrganization, LinkedAuthInterOrgs, MembershipFeesCondition, DebtModel, OverPaymentModel
from django.http import JsonResponse
from main.models import MainPageInfo


def register(request):
    form = CustomUserCreationForm()
    form.fields['auth_organization'].queryset = AuthOrganization.objects.filter(deleted=False)
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('main:home')

    context = {
        'forms': form
    }
    return render(request, 'users/register.html', context)


def login_by_imzo(request):
    inn = request.POST.get('tin')
    user = CustomUser.objects.get(tin=inn)
    login(request, user)
    return JsonResponse({'username': user.username, 'password': user.password}, status=200)


class UserLoginView(LoginView):
    template_name = 'users/login.html'
    success_url = reverse_lazy('main:home')


def eimzo(request):
    return render(request, 'users/imzo.html')


def profile_view(request, username):
    user_model = CustomUser.objects.get(username=username)
    user_org_id = request.user.auth_organization.id
    inter_orgs = LinkedAuthInterOrgs.objects.filter(auth_organization_id=user_org_id)
    membership_fees_count = MembershipFeesCondition.objects.filter(auth_organizations_id=user_org_id).count()
    debts_count = DebtModel.objects.filter(auth_organizations_id=user_org_id).count()
    over_payment_count = OverPaymentModel.objects.filter(auth_organizations_id=user_org_id).count()
    main_page_info = MainPageInfo.objects.filter(organization__auth_organization_id=user_org_id)
    context = {
        'user': user_model,
        'inter_orgs': inter_orgs,
        'membership_fees_count': membership_fees_count,
        'debts_count': debts_count,
        'over_payment_count': over_payment_count,
        'main_infos': main_page_info
    }
    return render(request, 'users/profile.html', context)


def logout_view(request):
    logout(request)
    return redirect('users:login')


class PasswordChangeView(PasswordChangeView):
    form_class = PasswordChangeForms
    template_name = 'users/password_change_form.html'
    success_url = reverse_lazy('users:password_change_done')


class PasswordChangeDoneView(PasswordChangeDoneView):
    template_name = 'users/password_change_done.html'
    form_class = PasswordChangeForms
