from django.urls import path, include
from .views import register, UserLoginView, profile_view, logout_view, PasswordChangeView, PasswordChangeDoneView, login_by_imzo
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_view


urlpatterns = [
    path('', UserLoginView.as_view(), name='login'),
    path('register/', register, name='register'),
    path('profile/<str:username>/', login_required(profile_view), name="profile"),
    # path('profile_image/', CreateProfileImage.as_view(), name='image'),
    path('logout/', login_required(logout_view), name="logout"),
    # password
    path('password_change/', PasswordChangeView.as_view(), name='password_change'),
    path('password_change_done/', PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('imzo/', login_by_imzo, name="e_imzo")

]
