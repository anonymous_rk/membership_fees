from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['auth_organization'].required = True
        self.fields['role'].required = True
        self.fields['password1'].label = 'Parol'
        self.fields['password2'].label = 'Parolni tasdiqlash'
        # self.fields['password1'].help_text = "Sizning parolingiz boshqa shaxsiy ma'lumotlaringizga aynan o'xshash bo'lmasligi kerak. Sizning parolingiz kamida 8 ta belgidan iborat bo'lishi kerak. Sizning parolingiz faqat raqamlardan iborat bo'lishi mumkin emas.",
        self.fields['password2'].help_text = "Parolni tasdiqlash uchun qayta kiriting."

    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'password1', 'password2', 'auth_organization', 'role']

        widgets = {
            'username': forms.TextInput(),
            'email': forms.EmailInput(),
            'password1': forms.PasswordInput(),
            'password2': forms.PasswordInput(),
            'auth_organization': forms.Select(attrs={
                # "class": "select2 ",
                # "data-width": '100%',
                # "style": 'width: 100%; height: 50px; !important',
            }),
            'role': forms.Select(),
        }

        labels = {
            'username': 'Username',
            'email': 'Email',
            'password1': 'Parol',
            'password2': 'Parolni tasdiqlash',
            'auth_organization': 'Vakolatli tashkilot',
            'role': 'Foydalanuvchi roli',
        }

        help_texts = {
            'username': "150 yoki undan kam harflar, raqamlar va faqat @/./+/-/_ belgilaridan foydalanish mumkin.",
            'email': 'Tashkilot elektron pochtasi.',
            'password1': "Sizning parolingiz boshqa shaxsiy ma'lumotlaringizga aynan o'xshash bo'lmasligi kerak."
                         "Sizning parolingiz kamida 8 ta belgidan iborat bo'lishi kerak."
                         "Sizning parolingiz faqat raqamlardan iborat bo'lishi mumkin emas.",
            'password2': 'Parolni tasdiqlash uchun qayta kiriting.',
            'auth_organization': "Foydalanuvchiga tegishli vakolatli tashkilotni tanlang",
            'role': 'Foydalanuvchi roli',
        }


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'password', 'auth_organization', 'role']


class UserLoginForm(forms.ModelForm):

    class Meta:
        model = CustomUser
        fields = ['username', 'password']


class PasswordChangeForms(PasswordChangeForm):
    old_password = forms.CharField(
        max_length=15,
        widget=forms.PasswordInput(attrs={'class': 'form_control', 'type': 'password'}),
        label='Joriy parol',
        help_text='Hozirgi parolingizni kiriting.'
    )
    new_password1 = forms.CharField(
        max_length=15,
        widget=forms.PasswordInput(attrs={'class': 'form_control', 'type': 'password'}),
        label='Yangi parol',
        help_text="Parol 8 tadan kam bo'lmagan harflar, raqamlar va faqat @/./+/-/_ belgilaridan iborat bo'lishi mumkin."
    )
    new_password2 = forms.CharField(
        max_length=15,
        widget=forms.PasswordInput(attrs={'class': 'form_control', 'type': 'password'}),
        label='Yangi parolni tasdiqlash',
        help_text="Parolni tasdiqlash uchun qayta kiriting."
    )

    class Meta:
        model = CustomUser
        fields = ['old_password', 'new_password1', 'new_password2']


class ProfileImageForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = [

            'image',

        ]
