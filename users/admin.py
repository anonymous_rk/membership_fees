from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['username', 'email', 'role']
    fieldsets = (
        (None, {'fields': ['username', 'password']}),
        ('Personal info', {'fields': ['first_name', 'last_name', 'email']}),
        ('Permissions', {'fields': ['is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions']}),
        ('Important dates', {'fields': ['last_login', 'date_joined']}),
        ('Specific', {'classes': ("wide",), 'fields': ['role', 'auth_organization', 'secret_key', 'image', 'tin']}),

    )
    add_fieldsets = (
        ('Main', {'fields': ['username', 'email', 'password1', 'password2']}),
        ('Personal info', {'fields': ['first_name', 'last_name']}),
        ('Permissions', {'fields': ['is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions']}),                                                                                                                               
        ('Specific', {'classes': ("wide",), 'fields': ['role', 'auth_organization', 'secret_key', 'image', 'tin']}),
    )


admin.site.register(CustomUser, CustomUserAdmin)
