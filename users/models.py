from django.db import models
from django.contrib.auth.models import AbstractUser
from organizations.models import AuthOrganization
import os
from uuid import uuid4
from django.utils.translation import gettext_lazy as _


def get_image_path(instance, filename):
    ext = str(filename).split('.')[-1]
    filename = f'{uuid4()}.{ext}'
    return os.path.join('user/', filename)


class CustomUser(AbstractUser):
    ROLES = [
        ("Admin", _("Admin")),
        ("Organization", _("Organization")),
        ("Monitoring", _("Monitoring"))
    ]

    auth_organization = models.ForeignKey(AuthOrganization, on_delete=models.CASCADE, default=1, blank=True, null=True)
    secret_key = models.CharField(max_length=500, null=True, blank=True)
    image = models.ImageField(upload_to=get_image_path, null=True)
    role = models.CharField(choices=ROLES, max_length=15, default="Organization")
    tin = models.CharField(max_length=9, null=True)


