from .models import InterOrganization
from django.http import JsonResponse


def get_currency(request):
    org_id = request.GET.get('org_id')
    model = InterOrganization.objects.get(deleted=False, id=org_id)
    print(model)
    data = "------"
    if org_id and model:
        data = model.currency.abbr
    result = {
        'currency_name': data
    }
    return JsonResponse(result)
