from django import template
from ..models import DebtModel, OverPaymentModel, InterOrganization
from django.utils.translation import gettext_lazy as _
register = template.Library()


@register.simple_tag()
def debt_filter(inter_organization, year):
    debt = DebtModel.objects.filter(inter_organization=inter_organization, fiscal_year=year)
    if debt:
        return debt
    return '-'