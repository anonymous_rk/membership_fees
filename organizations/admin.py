from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import AuthOrganization, InterOrganization, Currencies, LinkedAuthInterOrgs, MembershipFeesCondition, Birja


admin.site.register(AuthOrganization)
admin.site.register(InterOrganization)
admin.site.register(Currencies)
admin.site.register(LinkedAuthInterOrgs)
admin.site.register(MembershipFeesCondition)
admin.site.register(Birja)




