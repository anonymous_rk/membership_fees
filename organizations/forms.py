from django import forms
from .models import AuthOrganization, InterOrganization, LinkedAuthInterOrgs, Currencies, MembershipFeesCondition, DebtModel, OverPaymentModel
from django.utils.translation import gettext_lazy as _


class AuthOrgsForm(forms.ModelForm):
    class Meta:
        model = AuthOrganization
        fields = "__all__"

        widgets = {
            'name_uz': forms.TextInput(attrs={
                # "class": "my-3",
                "style": "height:50px"
            }),
            'name_ru': forms.TextInput(attrs={
                # "class": "my-3",
                "style": "height:50px"
            }),
            'name_en': forms.TextInput(attrs={
                # "class": "my-3",
                "style": "height:50px"
            }),
            'address_uz': forms.TextInput(attrs={
                # "class": "my-3",
                "style": "height:50px"
            }),
            'address_ru': forms.TextInput(attrs={
                # "class": "my-3",
                "style": "height:50px"
            }),
            'address_en': forms.TextInput(attrs={
                # "class": "my-3",
                "style": "height:50px"
            }),
            "email": forms.EmailInput(attrs={
                # "class": "my-1",
                "style": "height:50px"
            }),
            "phone_number": forms.TextInput(attrs={
                "placeholder": "+998999999999",
                # "class": "my-1",
                "style": "height:50px",
            }),
            'chief_accountant': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'chief_accountant_phone': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'international_department_staff': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'inter_dp_phone': forms.TextInput(attrs={
                "style": "height:50px",
            }),
        }

        labels = {
            "name_uz": _("Tashkilot nomi (uz)"),
            "name_ru": _("Tashkilot nomi (ru)"),
            "name_en": _("Tashkilot nomi (ўз)"),
            "address_uz": _("Manzil (uz)"),
            "address_ru": _("Manzil (ru)"),
            "address_en": _("Manzil (ўз)"),
            "email": _("Elektron pochta"),
            "phone_number": _("Telefon raqam"),
            "chief_accountant": _("Bosh hisobchi"),
            "chief_accountant_phone": _("Telefon raqam"),
            "international_department_staff": _("Xalqaro bo'lim hodimi"),
            "inter_dp_phone": _("Telefon raqam"),
        }


class InterOrgsForm(forms.ModelForm):
    class Meta:
        model = InterOrganization
        fields = "__all__"

        widgets = {
            "name_uz": forms.TextInput(attrs={
                'placeholder': 'Interpol',
                "style": "height:50px",
            }),
            "name_ru": forms.TextInput(attrs={
                'placeholder': 'Интерпол',
                "style": "height:50px",
            }),
            "name_en": forms.TextInput(attrs={
                'placeholder': 'Interpol',
                "style": "height:50px",
            }),
            "beneficiary_bank_name": forms.TextInput(attrs={
                'placeholder': '',
                "style": "height:50px",
            }),
            # bbank -> beneficiary bank
            "bbank_address_uz": forms.TextInput(attrs={
                'placeholder': '',
                "style": "height:50px",
            }),
            "bbank_account_name": forms.TextInput(attrs={
                'placeholder': '',
                "style": "height:50px",
            }),
            "bbank_account_number": forms.TextInput(attrs={
                'placeholder': '',
                "style": "height:50px",
            }),
            'funding_source': forms.Select(attrs={
                "style": "height:50px"
            }),
            'currency': forms.Select(attrs={
                "style": "height:50px",
            }),
        }

        labels = {
            "name_uz": _("Tashkilot nomi (uz)"),
            "name_ru": _("Tashkilot nomi (ru)"),
            "name_en": _("Tashkilot nomi (ўз)"),
            "beneficiary_bank_name": _("Benefitsiar bank nomi"),
            # bbank -> beneficiary bank
            "bbank_address_uz": _("Benefitsiar bank manzili"),
            "bbank_account_name": _("Benefitsiar bank hisob nomi"),
            "bbank_account_number": _("Benefitsiar bank hisob raqami"),
            "funding_source": _("Moliyalashtirish manbaasi"),
            'currency': _("Pul birligi"),
        }


class CurrencyForm(forms.ModelForm):
    class Meta:
        model = Currencies
        fields = '__all__'

        labels = {
            "name_uz": _("Nomi (uz)"),
            "name_ru": _("Nomi (ru)"),
            "name_en": _("Nomi (ўз)"),
            'sign': _("Belgi"),
            'abbr': _("Abbr")
        }


class MembershipFeeConditionForm(forms.ModelForm):

    class Meta:
        model = MembershipFeesCondition
        fields = '__all__'

        widgets = {
            'auth_organizations': forms.Select(attrs={
                "style": "height:50px",
            }),
            'inter_organization': forms.Select(attrs={
                "style": "height:50px",
            }),
            'fiscal_year': forms.Select(attrs={
                "id": "fiscal_year_id",
                "class": "select2bs4  fiscal_year",
                "data-width": '100%',
                "style": "height:50px",
                "aria-hidden": 'true',
                "data-select2-id": '1',
                "tabindex": "-1",
            }),
            'initial_debt': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'forecast_fee': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'sent_invoice_sum': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'paid_fee': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'bank_service': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'payment_confirmed': forms.CheckboxInput(attrs={
                # "style": "height:50px",
            }),
            'confirmation_data': forms.FileInput(attrs={
                # "class": "custom-file-input",
                # "style": "height:50px",

            }),
            'invoice_confirmation': forms.FileInput(attrs={
                # "style": "height:50px",
            }),
            'paid_fee_confirmation': forms.FileInput(attrs={
                # "style": "height:50px",
            }),
            'final_debt': forms.TextInput(attrs={
                "style": "height:50px",
            }),
            'payment_date': forms.DateInput(
                format='%d/%m/%Y',
                attrs={
                    'style': "height:50px",
                    'class': 'form-control',
                    'placeholder': 'Select a date',
                    'type': 'date'
                }
            )

        }

        labels = {
            'auth_organizations': _("Vakolatli tashkilot"),
            'inter_organization': _("Xalqaro tashkilot"),
            'fiscal_year': _("Moliya yili"),
            'initial_debt': _("Yil boshida qarzdorlik summasi"),
            'forecast_fee': _("A'zolik badali to'lovining prognozi summasi"),
            'sent_invoice_sum': _("Joriy yil uchun yuborilgan invoys summasi"),
            'paid_fee': _("Joriy yilda to'langan a'zolik badali summasi"),
            'bank_service': _("Bank xizmati"),
            'payment_confirmed': _("Badal summasi yetib borganligini tasdiqlash"),
            'confirmation_data': _("To'lov yetib borganligini tasdiqlang"),
            'final_debt': _("Yil oxirida qarzdorlik summasi"),
            'invoice_confirmation': _('Invoysni tasdiqlang'),
            'paid_fee_confirmation': _("To'lovni tasdiqlang"),
            'payment_date': _("To'lov sanasi")
        }


class LinkedOrgsModelForm(forms.ModelForm):
    class Meta:
        model = LinkedAuthInterOrgs
        fields = '__all__'

        widgets = {
            'auth_organization': forms.Select(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'inter_organization': forms.Select(
                attrs={
                    'class': "form-control-lg",
                    "required": False
                }
            ),
        }

        labels = {
            'auth_organization': _("Vakolatli tashkilot"),
            'inter_organization': _("Xalqaro tashkilot"),
        }


class DebtModelForm(forms.ModelForm):
    class Meta:
        model = DebtModel
        fields = '__all__'

        widgets = {
            'auth_organizations': forms.Select(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'inter_organization': forms.Select(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'fiscal_year': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                    'disabled': True
                }
            ),
            'final_debt': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                    'disabled': True
                }
            ),
            'initial_debt': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                    'disabled': True
                }
            ),
            'current_1': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'current_2': forms.TextInput(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'current_3': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'current_4': forms.TextInput(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'previous_years': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'paid_from_budget': forms.TextInput(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'paid_not_from_budget': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'other_sources': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            # 'final_debt': forms.TextInput(
            #     attrs={
            #         'class': 'form-control-lg'
            #     }
            # )
        }

        labels = {
            'auth_organizations': _("Vakolatli tashkilot"),
            'inter_organization': _("Xalqaro tashkilot"),
            'fiscal_year': _("Moliya yili"),
            'final_debt': _("Yil oxiridagi qarzdorlik"),
            'initial_debt': _("Yil boshidagi qarzdorlik"),
            'paid_from_budget': _("Budjet hisobidan"),
            'paid_not_from_budget': _("Budjetdan tashqari"),
            'other_sources': _("Boshqa manbaalar"),
            'previous_years': _("Avvalgi yillar")
        }


class OverPaymentModelForm(forms.ModelForm):
    class Meta:
        model = OverPaymentModel
        fields = '__all__'

        widgets = {
            'auth_organizations': forms.Select(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'inter_organization': forms.Select(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'fiscal_year': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                    'disabled': True
                }
            ),
            'final_amount': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                    'disabled': True
                }
            ),
            'initial_amount': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                    'disabled': True
                }
            ),
            'current_1': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'current_2': forms.TextInput(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'current_3': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'current_4': forms.TextInput(
                attrs={
                    'class': "form-control-lg"
                }
            ),
            'previous_years': forms.TextInput(
                attrs={
                    'class': "form-control-lg",
                }
            ),
            'taken_account': forms.TextInput(
                attrs={
                    'class': "form-control-lg"
                }
            ),
        }

        labels = {
            'auth_organizations': _("Vakolatli tashkilot"),
            'inter_organization': _("Xalqaro tashkilot"),
            'fiscal_year': _("Moliya yili"),
            'final_amount': _("Yil oxiridagi haqdorlik"),
            'initial_amount': _("Yil boshidagi haqdorlik"),
            'taken_account': _("Joriy yil to'lovlari hisobiga inobatga olingan"),
            'previous_years': _("Avvalgi yillar")
        }