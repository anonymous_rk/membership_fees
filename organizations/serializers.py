from rest_framework import serializers
from .models import Birja


class BirjaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Birja
        fields = ['bearer']
