from django.contrib.auth.decorators import login_required
from django.http import request

from .views import *
from django.urls import path, include
from .helpers import get_currency


urlpatterns = [
    path('local-organizations/', login_required(AuthOrgListView.as_view()), name="auth_orgs"),
    path('create-local-org/', login_required(AuthOrgCreateView.as_view()), name="auth_create"),
    path('local-org-detail/<int:id>/', login_required(AuthOrganizationDetail.as_view()), name='auth_detail'),
    path('update-local-org/<int:id>/', login_required(AuthOrganUpdate.as_view()), name="auth_update"),
    path('delete-local-org/<int:id>/', login_required(authorgan_delete), name="auth_delete"),
    # end AuthOrgan

    # start InterOrgan
    path('inter-organizations/', login_required(InterOrgListView.as_view()), name="inter_orgs"),
    path('inter_detail/<int:id>/', login_required(InterOrganDetail.as_view()), name='inter_detail'),
    path('inter_create/', login_required(InterOrganCreate.as_view()), name='inter_create'),
    path('inter_update/<int:id>/', login_required(InterOrganUpdate.as_view()), name='inter_update'),
    path('inter_delete/<int:id>/', login_required(inter_organ_delete), name='inter_delete'),
    # end InterOrgan

    # start Currency Part
    path('currency/', login_required(CurrencyListView.as_view()), name="currencies"),
    path('currency_detail/<int:id>/', login_required(CurrencyDetail.as_view()), name='currency_detail'),
    path('currency_create/', login_required(CurrencyCreate.as_view()), name='currency_create'),
    path('currency_update/<int:id>/', login_required(CurrensyUpdate.as_view()), name='currency_update'),
    path('currency_delete/<int:id>/', login_required(currency_delete), name='currency_delete'),
    # End Currency part

    # Start MemberShipFee Part
    path('membership-fee/in-budget/', login_required(MembershipFeeConditionListViewInBudget.as_view()),
         name="membership_fees"),
    path('membership-fee/out-budget/', login_required(MembershipFeeConditionListViewOutBudget.as_view()),
         name="membership_fee_out"),

    path('membership_detail/<int:id>/', login_required(MemberShipDetail.as_view()), name='membership_detail'),
    path('membership_create/', login_required(MembershipFeeConditionCreateView.as_view()), name="membership_create"),
    path('member_update/<int:id>/', login_required(MemberShipUpdate.as_view()), name='membership_update'),
    path('member_delete/<int:id>/', login_required(member_delete), name='membership_delete'),
    # End MemberShipFee

    # // Start LinkedOrgs urls \\#
    path('linked-orgs/out-budget', login_required(LinkedOrgsListView.as_view()), name="linked_orgs"),
    path('linked-orgs/in-budget', login_required(LinkedOrgsListViewInBudget.as_view()), name="linked_orgs_in"),
    path('linked-org-create/', login_required(LinkedOrgsCreateView.as_view()), name="linked_org_create"),
    path('linked-org-update/<int:id>/', login_required(LinkedOrgsUpdateView.as_view()), name="linked_org_update"),
    path('linked-org-detail/<int:id>/', login_required(LinkedOrgsDetailView.as_view()), name="linked_org_detail"),
    path('linked-org-delete/<int:id>/', login_required(linked_org__delete), name="linked_org_delete"),
    # end linkedorgs

    # Start debts urls
    path('debts/<int:year>/', login_required(DebtListView.as_view()), name="debts_list"),
    path('debts-create/<int:year>/', login_required(DebtCreateView.as_view()), name="debt_create"),
    path('debts-update/<int:year>/<int:id>/', login_required(DebtUpdateView.as_view()), name="debt_update"),
    path('debts-delete/<int:year>/<int:id>/', login_required(debt_delete), name="debt_delete"),
    path('debts-detail/<int:year>/<int:id>/', login_required(DebtDetailView.as_view()), name="debt_detail"),
    # end debts urls

    # Start over payments
    path('over-payments/<int:year>/', login_required(OverPaymentListView.as_view()), name="over_payments_list"),
    path('over-payment-create/<int:year>/', login_required(OverPaymentCreateView.as_view()),
         name="over_payment_create"),
    path('over-payment-update/<int:year>/<int:id>/', login_required(OverPaymentUpdateView.as_view()),
         name="over_payment_update"),
    path('over-payment-delete/<int:year>/<int:id>/', login_required(over_payment_delete), name="over_payment_delete"),
    path('over-payment-detail/<int:year>/<int:id>/', login_required(OverPaymentDetailView.as_view()),
         name="over_payment_detail"),

    #  end Payments

    # Helpers

    path('inter-org-currency/', login_required(get_currency), name='inter_org_currency')
]
