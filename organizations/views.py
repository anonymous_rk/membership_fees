from django.shortcuts import render, get_object_or_404, redirect
from .models import AuthOrganization, InterOrganization, Currencies, LinkedAuthInterOrgs, MembershipFeesCondition, \
    DebtModel, OverPaymentModel
from django.views.generic import ListView, CreateView, UpdateView, DetailView
from .forms import AuthOrgsForm, LinkedOrgsModelForm, InterOrgsForm, CurrencyForm, \
    MembershipFeeConditionForm, DebtModelForm, OverPaymentModelForm
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.utils.timezone import now
from django.core.cache import cache

CURRENT_DATE_TIME = now().strftime('%Y-%m-%d, %H:%M:%S')


class AuthOrgListView(ListView, PermissionRequiredMixin):
    """AuthOrganization Listview"""
    login_url = '/users/login/'
    model = AuthOrganization.objects.filter(deleted=False).order_by('-id')
    template_name = 'organizations/auth_orgs.html'
    permission_required = 'organizations.view_authorganization'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(AuthOrgListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.model

        context = {
            'object_list': query,

        }
        return render(request, self.template_name, context)


class AuthOrgCreateView(CreateView, PermissionRequiredMixin):
    """ InterOrganization Create part """
    login_url = '/users/login/'
    form_class = AuthOrgsForm
    model = AuthOrganization
    template_name = 'organizations/auth_org_create.html'
    permission_required = 'organizations.add_authorganization'
    raise_exception = True
    permission_denied_message = "You are not permitted to do this"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(AuthOrgCreateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        query = self.model
        context = {
            'form': form,
            'object': query
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            ph = data.phone_number
            if len(ph) == 9 and ph.isnumeric():
                ph = "+998" + ph
            data.phone_number = ph
            data.save()
            cache.clear()
            return redirect('organizations:auth_orgs')

        form = self.form_class(data=request.POST)
        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class AuthOrganizationDetail(DetailView, PermissionRequiredMixin):
    """AuthOrganization Detail part"""
    login_url = '/users/login/'
    model = AuthOrganization
    permission_required = 'organizations.view_authorganization'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(AuthOrganizationDetail, self).dispatch(request, *args, **kwargs)

    def get(self, request, id, *args, **kwargs):
        query = self.model.objects.get(id=id)
        context = {
            'object': query
        }
        return render(request, 'organizations/auth_org_detail.html', context)


class AuthOrganUpdate(UpdateView, PermissionRequiredMixin):
    """ AuthOrganization update part"""
    login_url = '/users/login/'
    model = AuthOrganization
    form_class = AuthOrgsForm
    template_name = 'organizations/auth_org_update.html'
    permission_required = 'organizations.change_authorganization'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(AuthOrganUpdate, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        id = self.kwargs.get('id')
        obj = None
        if id is not None:
            obj = get_object_or_404(AuthOrganization, id=id)
        return obj

    def get(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = AuthOrgsForm(instance=query)
            context = {'form': form, 'object': query}
            return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = self.form_class(data=request.POST, instance=query)
            if form.is_valid():
                data = form.save(commit=False)
                data.save()
                return redirect('organizations:auth_orgs')

        return self.get(request, *args, **kwargs)


@permission_required("organizations.delete_authorganization", raise_exception=True)
@login_required(login_url='users:login')
def authorgan_delete(request, id):
    """ Authorganization delete part"""

    organ = get_object_or_404(AuthOrganization, id=id)
    if request.method == 'POST':
        organ.deleted = True
        organ.deleted_at = CURRENT_DATE_TIME
        organ.save()
        return redirect('organizations:auth_orgs')
    context = {'object': organ}
    return render(request, 'organizations/auth_orgs_delete.html', context)


# ####\\\\\  end Authorgan   /////##### #


####\\\\\     InterOrganization    /////#### #

class InterOrgListView(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = InterOrganization
    template_name = "inter_organizations/inter_orgs.html"
    permission_required = 'organizations.view_interorganization'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(InterOrgListView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(InterOrgListView, self).get_context_data(**kwargs)
        data = self.model.objects.filter(deleted=False)
        linked_org = LinkedAuthInterOrgs.objects.filter(auth_organization=self.request.user.auth_organization, deleted=False).values_list('inter_organization_id', flat=True)

        context = {
            'data': data,
            'allowed_permission_orgs': linked_org
        }

        return context


class InterOrganDetail(DetailView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = InterOrganization
    template_name = 'inter_organizations/inter_detail.html'
    permission_required = 'organizations.view_interorganization'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(InterOrganDetail, self).dispatch(request, *args, **kwargs)

    def get(self, request, id, *args, **kwargs):
        query = self.model.objects.get(id=id, deleted=False)
        linked_org = LinkedAuthInterOrgs.objects.filter(auth_organization=self.request.user.auth_organization,
                                                        deleted=False).values_list('inter_organization_id', flat=True)
        context = {
            'object': query,
            'inter_org_perms': linked_org
        }
        return render(request, self.template_name, context)


class InterOrganCreate(CreateView, PermissionRequiredMixin):
    """ InterOrganization Create part """
    login_url = '/users/login/'
    model = InterOrganization
    template_name = 'inter_organizations/inter_org_create.html'
    permission_required = 'organizations.add_interorganization'
    raise_exception = True
    form_class = InterOrgsForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(InterOrganCreate, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = InterOrgsForm()
        query = self.model
        context = {
            'form': form,
            'object': query
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = InterOrgsForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            return redirect('organizations:inter_orgs')

        return self.get(request, *args, **kwargs)


class InterOrganUpdate(UpdateView, PermissionRequiredMixin):
    """InterOrganization Update part"""
    login_url = '/users/login/'
    model = InterOrganization
    template_name = 'inter_organizations/inter_org_update.html'
    form_class = InterOrgsForm
    permission_required = 'organizations.change_interorganization'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(InterOrganUpdate, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        inter = None
        id = self.kwargs.get('id')
        if id is not None:
            inter = get_object_or_404(InterOrganization, id=id)
        return inter

    def get(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = InterOrgsForm(instance=query)
            context = {
                'object': query,
                'form': form
            }
            return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = InterOrgsForm(instance=query, data=request.POST, files=request.FILES)
            if form.is_valid():
                form.save()
                return redirect('organizations:inter_orgs')
        return self.get(request, *args, **kwargs)


@permission_required('organizations.delete_interorganization', raise_exception=True)
@login_required(login_url='users:login')
def inter_organ_delete(request, id):
    inter = get_object_or_404(InterOrganization, id=id)
    if request.method == 'POST':
        inter.deleted = True
        inter.deleted_at = CURRENT_DATE_TIME
        inter.save()
        messages.warning(request, 'Muvvafaqiyatli o\'chirildi')
        return redirect('organizations:inter_orgs')
    context = {'object': inter}
    return render(request, 'inter_organizations/inter_org_delete.html', context)


###///          end InterOrganization part      ////###############


###///          Start Currency Part     ////#########

class CurrencyListView(ListView, PermissionRequiredMixin):
    """Currensy Part """
    login_url = '/users/login/'
    queryset = Currencies.objects.order_by('-id').filter(deleted=False)
    model = Currencies
    template_name = "currencies/currencies.html"
    permission_required = 'organizations.view_currencies'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(CurrencyListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.queryset.filter(deleted=False)
        context = {'objects': query}
        return render(request, self.template_name, context)


class CurrencyDetail(DetailView, PermissionRequiredMixin):
    """Currensy Detail part"""
    login_url = '/users/login/'
    model = Currencies
    template_name = 'currencies/currency_detail.html'
    permission_required = 'organizations.view_currencies'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(CurrencyDetail, self).dispatch(request, *args, **kwargs)

    def get(self, request, id=None, *args, **kwargs):
        query = self.model.objects.get(id=id)
        context = {'object': query}
        return render(request, self.template_name, context)


class CurrencyCreate(CreateView, PermissionRequiredMixin):
    """Currensy Create part"""
    login_url = '/users/login/'
    model = Currencies
    template_name = 'currencies/currency_create.html'
    permission_required = 'organizations.add_currencies'
    raise_exception = True
    form_class = CurrencyForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(CurrencyCreate, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = CurrencyForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('organizations:currencies')

        form = self.form_class(data=request.POST)
        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class CurrensyUpdate(UpdateView, PermissionRequiredMixin):
    """Currensy Update Part"""
    login_url = '/users/login/'
    model = Currencies
    template_name = 'currencies/currency_update.html'
    form_class = CurrencyForm
    permission_required = 'organizations.change_currencies'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(CurrensyUpdate, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        curren = None
        id = self.kwargs.get('id')
        if id is not None:
            curren = get_object_or_404(Currencies, id=id)
        return curren

    def get(self, request, id=None, *args, **kwargs):
        curren = self.get_object()
        if curren is not None:
            form = CurrencyForm(instance=curren)
            context = {
                'form': form,
                'object': curren
            }
            return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        curren = self.get_object()
        if curren is not None:
            form = CurrencyForm(instance=curren, data=request.POST, files=request.FILES)
            if form.is_valid():
                form.save()
                return redirect('organizations:currencies')
        return self.get(request, *args, **kwargs)


@permission_required('organizations.delete_currencies', raise_exception=True)
@login_required(login_url='users:login')
def currency_delete(request, id):
    curren = get_object_or_404(Currencies, id=id)
    if request.method == 'POST':
        curren.deleted = True
        curren.deleted_at = CURRENT_DATE_TIME
        curren.save()
        messages.warning(request, "muvvafaqiyatli o'chirildi ")
        return redirect('organizations:currencies')
    context = {'object': curren}
    return render(request, 'currencies/currency_delete.html', context)


####////////       E    nd Currensy Part     /////#########


###//  Start MembershipFee Condition  Part   ///#######


class MembershipFeeConditionListViewInBudget(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    queryset = MembershipFeesCondition.objects.order_by('-id').filter(deleted=False)
    model = MembershipFeesCondition
    template_name = "membership_fee_condition/membership_fee.html"
    permission_required = 'organizations.view_membershipfeescondition'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MembershipFeeConditionListViewInBudget, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        obj = self.queryset.filter(inter_organization__funding_source='in_budget')
        if request.user.role == 'Organization':
            obj = self.queryset.filter(inter_organization__funding_source='in_budget',
                                       auth_organizations=request.user.auth_organization)

        context = {
            'objects': obj
        }
        return render(request, self.template_name, context)


class MembershipFeeConditionListViewOutBudget(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    queryset = MembershipFeesCondition.objects.order_by('-id').filter(deleted=False)
    template_name = "membership_fee_condition/out_budget.html"
    permission_required = 'organizations.view_membershipfeescondition'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MembershipFeeConditionListViewOutBudget, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        obj = self.queryset.filter(inter_organization__funding_source='out_budget')
        if request.user.role == "Organization":
            obj = self.queryset.filter(inter_organization__funding_source='out_budget',
                                       auth_organizations=request.user.auth_organization)

        context = {
            'objects': obj
        }
        return render(request, self.template_name, context)


class MemberShipDetail(DetailView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = MembershipFeesCondition
    template_name = 'membership_fee_condition/detail.html'
    permission_required = 'organizations.view_membershipfeescondition'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MemberShipDetail, self).dispatch(request, *args, **kwargs)

    def get(self, request, id=None, *args, **kwargs):
        query = self.model.objects.get(id=id)
        if request.user.role == "Organization":
            query = self.model.objects.get(id=id, auth_organizations=request.user.auth_organization)

        context = {'object': query}
        return render(request, self.template_name, context)


class MembershipFeeConditionCreateView(CreateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = MembershipFeesCondition
    form_class = MembershipFeeConditionForm
    template_name = "membership_fee_condition/create.html"
    permission_required = 'organizations.add_membershipfeescondition'
    raise_exception = True
    success_url = 'main:home'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MembershipFeeConditionCreateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.model
        form = self.form_class()

        form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(deleted=False)
        form.fields['inter_organization'].queryset = InterOrganization.objects.filter(deleted=False)
        if request.user.role != 'Admin':
            form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(
                id=request.user.auth_organization_id, deleted=False)
            form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(
                auth_organization_id=request.user.auth_organization_id, deleted=False)

        context = {
            'object': query,
            'form': form,
            'from': request.GET.get("from"),
        }

        if request.GET.get("from") == "in-budget":
            self.success_url = 'organizations:membership_fees'
        else:
            self.success_url = 'organizations:membership_fee_out'

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST, files=request.FILES)
        update_data = request.POST.copy()
        update_data.update({'auth_organizations': request.user.auth_organization})
        form = self.form_class(data=update_data, files=request.FILES)
        if form.is_valid():
            form.save()
            if InterOrganization.objects.get(name_uz=form.cleaned_data["inter_organization"]).funding_source == 'in_budget':
                return redirect('organizations:membership_fees')
            return redirect('organizations:membership_fee_out')

        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class MemberShipUpdate(UpdateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = MembershipFeesCondition
    template_name = 'membership_fee_condition/update.html'
    permission_required = 'organizations.change_membershipfeescondition'
    raise_exception = True
    form_class = MembershipFeeConditionForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(MemberShipUpdate, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        member = None
        id = self.kwargs.get('id')
        if id is not None:
            member = get_object_or_404(MembershipFeesCondition, id=id)
        return member

    def get(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = self.form_class(instance=query)
            # form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(deleted=False)
            # form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)
            # if not request.user.role != 'Admin':
            #     form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(
            #         id=request.user.auth_organization_id, deleted=False)
            #     form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(
            #         auth_organization_id=request.user.auth_organization_id, deleted=False)

            context = {
                'object_list': query,
                'form': form
            }
            return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        query = self.get_object()
        if query is not None:
            form = self.form_class(instance=query, data=request.POST, files=request.FILES)
            update_data = request.POST.copy()
            update_data.update({'auth_organizations': request.user.auth_organization})
            form = self.form_class(data=update_data, files=request.FILES, instance=query)
            if form.is_valid():
                form.save()
                if form.cleaned_data["inter_organization"].funding_source == 'in_budget':
                    return redirect('organizations:membership_fees')
                return redirect('organizations:membership_fee_out')

        context = {
            "object_list": query,
            "form": form
        }
        return render(request, self.template_name, context)


@permission_required('organizations.delete_membershipfeescondition', raise_exception=True)
@login_required(login_url='users:login')
def member_delete(request, id):
    query = get_object_or_404(MembershipFeesCondition, id=id)
    if request.method == 'POST':
        query.deleted = True
        query.save()
        return redirect('organizations:membership_fees')
    context = {'object': query}
    return render(request, 'membership_fee_condition/delete.html', context)


# #####\\\\\\      end MemberShipFee part     //////##### #


# #####\\\\\\      start LinkedOrgs part     //////##### #

class LinkedOrgsListView(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)
    template_name = "linked_organizations/linked_orgs.html"
    permission_required = 'organizations.view_linkedauthinterorgs'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(LinkedOrgsListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        obj = self.queryset.filter(inter_organization__funding_source='out_budget')

        context = {
            'object_list': obj,
        }
        return render(request, self.template_name, context)


class LinkedOrgsListViewInBudget(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = LinkedAuthInterOrgs.objects.filter(deleted=False)
    queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)
    template_name = "linked_organizations/in_budget.html"
    permission_required = 'organizations.view_linkedauthinterorgs'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(LinkedOrgsListViewInBudget, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        obj = self.queryset.filter(inter_organization__funding_source='in_budget')

        context = {'object_list': obj}
        return render(request, self.template_name, context)


class LinkedOrgsDetailView(DetailView, PermissionRequiredMixin):
    login_url = '/users/login/'
    # model = LinkedAuthInterOrgs.objects.all()
    queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)
    template_name = "linked_organizations/linked_org_detail.html"
    permission_required = 'organizations.view_linkedauthinterorgs'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(LinkedOrgsDetailView, self).dispatch(request, *args, **kwargs)

    def get(self, request, id=None, *args, **kwargs):
        obj = self.queryset.get(id=id)
        context = {'object': obj}
        return render(request, self.template_name, context)


class LinkedOrgsCreateView(CreateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = LinkedAuthInterOrgs.objects.filter(deleted=False)
    queryset = LinkedAuthInterOrgs.objects.order_by('-id').filter(deleted=False)
    template_name = "linked_organizations/linked_org_create.html"
    form_class = LinkedOrgsModelForm
    permission_required = 'organizations.add_linkedauthinterorgs'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(LinkedOrgsCreateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.model
        form = self.form_class()
        form.fields['auth_organization'].queryset = AuthOrganization.objects.filter(deleted=False)
        form.fields['inter_organization'].queryset = InterOrganization.objects.filter(deleted=False)
        context = {
            'object': query,
            'form': form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            form.save()
            if InterOrganization.objects.get(name_uz=form.cleaned_data['inter_organization']).funding_source == 'in_budget':
                return redirect('organizations:linked_orgs_in')
            return redirect('organizations:linked_orgs')

        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class LinkedOrgsUpdateView(UpdateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = LinkedAuthInterOrgs
    queryset = LinkedAuthInterOrgs.objects.order_by('-id')
    template_name = "linked_organizations/linked_org_update.html"
    form_class = LinkedOrgsModelForm
    permission_required = 'organizations.change_linkedauthinterorgs'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(LinkedOrgsUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, id, *args, **kwargs):
        query = self.model.objects.get(id=id)
        form = self.form_class(instance=query)
        # form.fields['auth_organization'].queryset = AuthOrganization.objects.filter(deleted=False)
        # form.fields['inter_organization'].queryset = InterOrganization.objects.filter(deleted=False)
        context = {
            'object': query,
            'form': form
        }
        return render(request, self.template_name, context)

    def post(self, request, id, *args, **kwargs):
        query = self.queryset.get(id=id)
        form = self.form_class(instance=query, data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if InterOrganization.objects.get(
                    name_uz=form.cleaned_data['inter_organization']).funding_source == 'in_budget':
                return redirect('organizations:linked_orgs_in')
            return redirect('organizations:linked_orgs')

        return self.get(self, request, *args, **kwargs)


@permission_required('organizations.delete_linkedauthinterorgs', raise_exception=True)
@login_required(login_url='users:login')
def linked_org__delete(request, id):
    query = get_object_or_404(LinkedAuthInterOrgs, id=id)
    if request.method == 'POST':
        query.deleted = True
        query.deleted_at = CURRENT_DATE_TIME
        query.save()
        return redirect('organizations:linked_orgs')
    context = {'object': query}
    return render(request, 'linked_organizations/linked_org_delete.html', context)


class DebtListView(ListView, PermissionRequiredMixin):
    """
    debt list
    """
    login_url = '/users/login/'
    queryset = DebtModel.objects.order_by('-id').filter(deleted=False)
    model = DebtModel
    template_name = "debts/debts_list.html"
    permission_required = 'organizations.view_debtmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(DebtListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, *args, **kwargs):
        obj = self.queryset.filter(fiscal_year=year)
        if request.user.role == 'Organization':
            obj = self.queryset.filter(fiscal_year=year, auth_organizations=request.user.auth_organization)
        context = {
            'objects': obj,
            "year": year
        }
        return render(request, self.template_name, context)


class DebtDetailView(DetailView, PermissionRequiredMixin):
    """
    debt detail
    """
    login_url = '/users/login/'
    queryset = DebtModel.objects.order_by('-id').filter(deleted=False)
    model = DebtModel
    template_name = "debts/debt_detail.html"
    permission_required = 'organizations.view_debtmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(DebtDetailView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, id, *args, **kwargs):
        obj = self.queryset.get(id=id, fiscal_year=year)
        if request.user.role == 'Organization':
            obj = self.queryset.get(id=id, fiscal_year=year, auth_organizations=request.user.auth_organization)
        context = {
            'o': obj,
            "year": year
        }
        return render(request, self.template_name, context)


class DebtCreateView(CreateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = DebtModel.objects.filter(deleted=False)
    queryset = DebtModel.objects.order_by('-id').filter(deleted=False)
    template_name = "debts/debt_create.html"
    form_class = DebtModelForm
    permission_required = 'organizations.add_debtmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(DebtCreateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, *args, **kwargs):
        query = self.model
        inter_org_currencies = query.filter(deleted=False).values('inter_organization__currency_id',
                                                                  "inter_organization__currency__name_uz",
                                                                  "inter_organization__currency__name_ru",
                                                                  "inter_organization__currency__name_en")
        form = self.form_class()
        form.fields["auth_organizations"].queryset = AuthOrganization.objects.filter(deleted=False)
        form.fields["inter_organization"].queryset = InterOrganization.objects.filter(deleted=False)
        if request.user.role != "Admin":
            form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(deleted=False,
                                                                                         id=request.user.auth_organization_id)
            form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False,
                                                                                            auth_organization_id=request.user.auth_organization_id)

        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4

        form.fields['fiscal_year'].value = year
        context = {
            'object': query,
            'form': form,
            'year': year,
            "inter_currencies": inter_org_currencies
        }
        return render(request, self.template_name, context)

    def post(self, request, year, *args, **kwargs):
        form = self.form_class(data=request.POST)
        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4

        update_data = request.POST.copy()
        update_data.update({'fiscal_year': year})
        update_data.update({'auth_organizations': request.user.auth_organization})
        form = self.form_class(data=update_data)

        if form.is_valid():
            data = form.save(commit=False)
            # data.created_at = CURRENT_DATE_TIME
            data.initial_debt = data.current_1 + data.current_2 + data.current_3 + data.current_4 + data.previous_years
            data.final_debt = data.initial_debt - (
                    data.paid_from_budget + data.paid_not_from_budget + data.other_sources)
            data.save()
            return redirect('organizations:debts_list', year=year)

        context = {
            'form': form,
            "year": year
        }

        return render(request, self.template_name, context)


class DebtUpdateView(UpdateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = DebtModel
    queryset = DebtModel.objects.order_by('-id')
    template_name = "debts/debt_update.html"
    form_class = DebtModelForm
    permission_required = 'organizations.change_debtmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(DebtUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, id, *args, **kwargs):
        query = self.model.objects.get(id=id, fiscal_year=year)
        form = self.form_class(instance=query)
        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4
        # form.fields["auth_organizations"].queryset = AuthOrganization.objects.filter(deleted=False)
        # form.fields["inter_organization"].queryset = InterOrganization.objects.filter(deleted=False)
        #
        # if request.user.role != "Admin":
        #     form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(deleted=False,
        #                                                                                  id=request.user.auth_organization_id)
        #     form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False,
        #                                                                                     auth_organization_id=request.user.auth_organization_id)

        currency = query.inter_organization.currency.abbr
        context = {
            'object': query,
            'form': form,
            "id": id,
            "year": year,
            'currency': currency
        }
        return render(request, self.template_name, context)

    def post(self, request, year, id, *args, **kwargs):
        query = self.queryset.get(id=id, fiscal_year=year)
        form = self.form_class(instance=query, data=request.POST)
        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4

        update_data = request.POST.copy()
        update_data.update({'fiscal_year': year})
        update_data.update({'auth_organizations': request.user.auth_organization})
        form = self.form_class(data=update_data, instance=query)

        if form.is_valid():
            data = form.save(commit=False)
            data.initial_debt = data.current_1 + data.current_2 + data.current_3 + data.current_4 + data.previous_years
            data.final_debt = data.initial_debt - (
                    data.paid_from_budget + data.paid_not_from_budget + data.other_sources)
            data.save()
            return redirect('organizations:debts_list', year=year)
        context = {
            "form": form,
            "object": query,
            "id": id,
            "year": year
        }

        return render(request, self.template_name, context)


@permission_required('organizations.delete_debtmodel', raise_exception=True)
@login_required(login_url='users:login')
def debt_delete(request, year, id):
    query = get_object_or_404(DebtModel, id=id, fiscal_year=year)
    if request.method == 'POST':
        query.deleted = True
        query.deleted_at = CURRENT_DATE_TIME
        query.save()
        return redirect('organizations:debts_list', year=year)
    context = {'object': query, "year": year}
    return render(request, 'debts/debt_delete.html', context)


#  Over payment views
class OverPaymentListView(ListView, PermissionRequiredMixin):
    """
    over payment list
    """
    login_url = '/users/login/'
    queryset = OverPaymentModel.objects.order_by('-id').filter(deleted=False)
    model = OverPaymentModel
    template_name = "over_paid/over_payments_list.html"
    permission_required = 'organizations.view_overpaymentmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(OverPaymentListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, *args, **kwargs):
        obj = self.queryset.filter(fiscal_year=year)
        if request.user.role == 'Organization':
            obj = self.queryset.filter(fiscal_year=year, auth_organizations=request.user.auth_organization)
        context = {
            'objects': obj,
            "year": year
        }
        return render(request, self.template_name, context)


class OverPaymentDetailView(DetailView, PermissionRequiredMixin):
    """
    over payment detail
    """
    login_url = '/users/login/'
    queryset = OverPaymentModel.objects.order_by('-id').filter(deleted=False)
    model = OverPaymentModel
    template_name = "over_paid/over_paid_detail.html"
    permission_required = 'organizations.view_overpaymentmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(OverPaymentDetailView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, id, *args, **kwargs):
        obj = self.queryset.get(id=id, fiscal_year=year)
        if request.user.role == 'Organization':
            obj = self.queryset.get(id=id, fiscal_year=year, auth_organizations=request.user.auth_organization)
        context = {
            'o': obj,
            "year": year
        }
        return render(request, self.template_name, context)


class OverPaymentCreateView(CreateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = OverPaymentModel.objects.filter(deleted=False)
    template_name = "over_paid/over_paid_create.html"
    form_class = OverPaymentModelForm
    permission_required = 'organizations.add_overpaymentmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(OverPaymentCreateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, *args, **kwargs):
        query = self.model
        form = self.form_class()
        form.fields["auth_organizations"].queryset = AuthOrganization.objects.filter(deleted=False)
        form.fields["inter_organization"].queryset = InterOrganization.objects.filter(deleted=False)

        if request.user.role != "Admin":
            form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(deleted=False,
                                                                                         id=request.user.auth_organization_id)
            form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False,
                                                                                            auth_organization_id=request.user.auth_organization_id)

        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4

        form.fields['fiscal_year'].value = year
        context = {
            'object': query,
            'form': form,
            'year': year
        }
        return render(request, self.template_name, context)

    def post(self, request, year, *args, **kwargs):
        form = self.form_class(data=request.POST)
        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4

        update_data = request.POST.copy()
        update_data.update({'fiscal_year': year})
        update_data.update({'auth_organizations': request.user.auth_organization})
        form = self.form_class(data=update_data)

        if form.is_valid():
            data = form.save(commit=False)
            data.initial_amount = data.current_1 + data.current_2 + data.current_3 + data.current_4 + data.previous_years
            data.final_amount = data.initial_amount - data.taken_account
            data.save()
            return redirect('organizations:over_payments_list', year=year)

        context = {
            'form': form,
            "year": year
        }

        return render(request, self.template_name, context)


class OverPaymentUpdateView(UpdateView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = OverPaymentModel
    queryset = OverPaymentModel.objects.order_by('-id')
    template_name = "over_paid/over_paid_update.html"
    form_class = OverPaymentModelForm
    permission_required = 'organizations.change_overpaymentmodel'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(OverPaymentUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, year, id, *args, **kwargs):
        query = self.model.objects.get(id=id, fiscal_year=year)
        form = self.form_class(instance=query)
        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4
        # form.fields["auth_organizations"].queryset = AuthOrganization.objects.filter(deleted=False)
        # form.fields["inter_organization"].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False)
        #
        # if request.user.role != "Admin":
        #     form.fields['auth_organizations'].queryset = AuthOrganization.objects.filter(deleted=False,
        #                                                                                  id=request.user.auth_organization_id)
        #     form.fields['inter_organization'].queryset = LinkedAuthInterOrgs.objects.filter(deleted=False,
        #                                                                                     auth_organization_id=request.user.auth_organization_id)

        currency = query.inter_organization.currency.abbr
        context = {
            'object': query,
            'form': form,
            "id": id,
            "year": year,
            "currency": currency
        }
        return render(request, self.template_name, context)

    def post(self, request, year, id, *args, **kwargs):
        query = self.model.get(id=id, fiscal_year=year)
        form = self.form_class(instance=query, data=request.POST)
        form.fields['current_1'].label = year - 1
        form.fields['current_2'].label = year - 2
        form.fields['current_3'].label = year - 3
        form.fields['current_4'].label = year - 4

        update_data = request.POST.copy()
        update_data.update({'fiscal_year': year})
        update_data.update({'auth_organizations': request.user.auth_organization})
        form = self.form_class(data=update_data, instance=query)

        if form.is_valid():
            data = form.save(commit=False)
            data.initial_amount = data.current_1 + data.current_2 + data.current_3 + data.current_4 + data.previous_years
            data.final_amount = data.initial_amount - data.taken_account
            data.save()
            return redirect('organizations:over_payments_list', year=year)
        context = {
            "form": form,
            "object": query,
            "id": id,
            "year": year
        }

        return render(request, self.template_name, context)


@permission_required('organizations.delete_overpaymentmodel', raise_exception=True)
@login_required(login_url='users:login')
def over_payment_delete(request, year, id):
    query = get_object_or_404(OverPaymentModel, id=id, fiscal_year=year)
    if request.method == 'POST':
        query.deleted = True
        query.deleted_at = CURRENT_DATE_TIME
        query.save()
        return redirect('organizations:over_payments_list', year=year)
    context = {'object': query, "year": year}
    return render(request, 'over_paid/over_paid_delete.html', context)
