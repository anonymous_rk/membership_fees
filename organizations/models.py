from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from .validators import number_validator, phone_nb_validator, currency_validator, file_validator
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.utils.timezone import now

CURRENT_DATE = str(now().strftime('%Y-%m-%d, %H:%M'))


class AuthOrganization(models.Model):

    """
    Vakolatli tashkilot ma'lumotlari
    """

    # Moliyalshtirish manbaasi turlari

    name_uz = models.CharField(max_length=500)
    name_ru = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)
    address_uz = models.CharField(max_length=255)
    address_ru = models.CharField(max_length=255)
    address_en = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, help_text=_("Email address of authorized organization."))
    phone_number = models.CharField(max_length=13,
                                    validators=[MaxLengthValidator(13), MinLengthValidator(9), phone_nb_validator],
                                    default="+998", help_text=_("Phone number of authorized organization."))
    tax_number = models.CharField(unique=True, max_length=9, null=True, blank=True,
                                  validators=[number_validator])  # Validate!
    inn_code = models.CharField(unique=True, max_length=9, null=True, blank=True,
                                validators=[number_validator])  # Validate!
    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)

    chief_accountant = models.CharField(max_length=50,
                                        help_text=_("Full name of chief accountant of authorized organization."))
    chief_accountant_phone = models.CharField(max_length=13, validators=[MaxLengthValidator(13), MinLengthValidator(9),
                                                                         phone_nb_validator], default="+998",
                                              help_text=_("Chief accountant phone number."))
    international_department_staff = models.CharField(max_length=50, help_text=_("Staff of international department."))
    inter_dp_phone = models.CharField(max_length=13,
                                      validators=[MaxLengthValidator(13), MinLengthValidator(9), phone_nb_validator],
                                      default="+998", help_text=_("Phone number of international department."))

    def __str__(self):
        return str(self.name_uz)

    class Meta:
        verbose_name = _("Authorized organization")
        verbose_name_plural = _("Authorized organizations")


class Currencies(models.Model):
    """
    Pul birliklari
    """

    name_uz = models.CharField(max_length=25)
    name_ru = models.CharField(max_length=25)
    name_en = models.CharField(max_length=25)
    sign = models.CharField(max_length=1, help_text=_("Currency sign, such as $, €."))
    abbr = models.CharField(max_length=3, validators=[currency_validator],
                            help_text=_("Input the abbreviation for currency."))
    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)

    def __str__(self):
        return f"{self.name_uz} {self.sign}"

    class Meta:
        verbose_name = "Currency"
        verbose_name_plural = "Currencies"


class InterOrganization(models.Model):
    """
    Xalqaro tashkilot ma'lumotlari
    """

    name_uz = models.CharField(max_length=500)
    name_ru = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)
    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)

    # Benefitsiar bank ma'lumotlari

    beneficiary_bank_name = models.CharField(max_length=50, help_text=_("Beneficiary bank's name."))
    bbank_address_uz = models.CharField(max_length=255, help_text=_("Beneficiary bank's address."))
    # bbank_address_ru = models.CharField(max_length=255, help_text=_("Beneficiary bank's address."))
    # bbank_address_en = models.CharField(max_length=255, help_text=_("Beneficiary bank's address."))
    bbank_account_name = models.CharField(max_length=50, help_text=_("Beneficiary bank's account name."))
    bbank_account_number = models.CharField(max_length=50, help_text=_("Beneficiary bank's account number."))
    currency = models.ForeignKey(Currencies, on_delete=models.CASCADE,
                                 help_text=_("The currency unit in which the payment is made"))
    FUNDING_SOURCE_CHOICES = (
        ("in_budget", "Byudjet"),
        ("out_budget", "Byudjetdan tashqari")
    )

    funding_source = models.CharField(max_length=20, choices=FUNDING_SOURCE_CHOICES,
                                      help_text=_("Type of financing resource."))

    # email = models.EmailField(max_length=255)
    # website_url = models.URLField(max_length=255)
    # phone_number = models.CharField(max_length=20)

    def __str__(self):
        return str(self.name_uz)

    class Meta:
        verbose_name = _("International organization")
        verbose_name_plural = _("International organizations")


class LinkedAuthInterOrgs(models.Model):
    """
    O'zbekiston Respublikasining xalqaro shartnomalar
    va xalqaro tashkilotlarga a'zolik bo'yicha
    majburiyatlarni bajarishga mas'ul davlat organlari
    va boshqa tashkilotlar
    """

    auth_organization = models.ForeignKey(AuthOrganization, on_delete=models.CASCADE,
                                          help_text=_("Authorized local organization."))
    inter_organization = models.ForeignKey(InterOrganization, on_delete=models.CASCADE,
                                           help_text=_("International organization."), blank=True, null=True)
    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)

    def __str__(self):
        return str(self.inter_organization)

    class Meta:
        verbose_name = _("Local and its belonging organization")
        verbose_name_plural = _("Local and its belonging organizations")


class MembershipFeesCondition(models.Model):
    """
    O'zbekiston Respublikasining xalqaro shartnomalar
    va xalqaro tashkilotlarga a'zolik bo'yicha
    mas'ul davlat organlari va boshqa tashkilotlarning
     a'zolik badallarining to'lovi ma'lumotlari
    """
    YEAR_CHOICES = [(f'{y}', f'{y}') for y in range(1991, timezone.now().year + 10)]

    # organizations = models.ForeignKey(LinkedAuthInterOrgs, on_delete=models.CASCADE, default=0)
    auth_organizations = models.ForeignKey(AuthOrganization, on_delete=models.CASCADE, default=1)
    inter_organization = models.ForeignKey(InterOrganization, on_delete=models.CASCADE, default=1)
    fiscal_year = models.CharField(choices=YEAR_CHOICES, help_text=_("Fiscal year"), max_length=4)

    forecast_fee = models.DecimalField(help_text=_("The forecast amount of the membership fee payment"),
                                       decimal_places=2, max_digits=15)
    sent_invoice_sum = models.DecimalField(help_text=_("The amount of invoice sent for the current year"),
                                           decimal_places=2, blank=True, null=True, max_digits=15)
    invoice_confirmation = models.FileField(help_text=_(
        "File on confirmation of  sent invoice  from international organization."), blank=True, null=True,
                                            upload_to='confirmations_invoices/', validators=[file_validator])
    paid_fee = models.DecimalField(help_text=_("The amount of membership fee paid in the current year"),
                                   decimal_places=2, max_digits=15, null=True, blank=True)
    paid_fee_confirmation = models.FileField(help_text=_("File on confirmation of paid fee from local organization."),
                                             upload_to='confirmation_payments/', blank=True, null=True, validators=[file_validator])
    bank_service = models.DecimalField(help_text=_("Bank service fee"), default=0.00, blank=True, null=True, decimal_places=2, max_digits=15)
    payment_confirmed = models.BooleanField(default=False, blank=True, null=True,)
    confirmation_data = models.FileField(upload_to='confirmation_emails/', blank=True, null=True, help_text=_(
        "Information on confirmation of receipt of the amount of membership fee to the international organization"),
                                         validators=[file_validator])  # validate!

    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)
    create_at = models.DateTimeField(null=True, auto_now_add=True)
    created_by = models.CharField(blank=True, null=True, default='NOT CREATED BY USER', max_length=25)
    payment_date = models.DateField(default=CURRENT_DATE.split(' ')[0].strip(','), blank=True, null=True)

    def __str__(self):
        return f"{self.auth_organizations} - {self.paid_fee}"

    class Meta:
        verbose_name = _("Membership fee condition")
        verbose_name_plural = _("Membership fees condition")


class DebtModel(models.Model):
    """
    Qarzdorlik bo'yicha ma'lumot
    """
    auth_organizations = models.ForeignKey(AuthOrganization, on_delete=models.CASCADE, default=1)
    inter_organization = models.ForeignKey(InterOrganization, on_delete=models.CASCADE, default=1)
    initial_debt = models.DecimalField(decimal_places=2, max_digits=12, blank=True)
    final_debt = models.DecimalField(decimal_places=2, max_digits=15, blank=True)
    fiscal_year = models.CharField(default=str(now().year), help_text=_("Fiscal year"), max_length=4)
    created_at = models.DateTimeField(auto_created=True, blank=True, null=True)
    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)

    #  Up to years
    current_1 = models.IntegerField(default=0, blank=True)
    current_2 = models.IntegerField(default=0, blank=True)
    current_3 = models.IntegerField(default=0, blank=True)
    current_4 = models.IntegerField(default=0, blank=True)
    previous_years = models.IntegerField(default=0, blank=True)

    #  Paid fees in the current year
    paid_from_budget = models.IntegerField(default=0, blank=True)
    paid_not_from_budget = models.IntegerField(default=0, blank=True)
    other_sources = models.IntegerField(default=0, blank=True)


class OverPaymentModel(models.Model):
    """
    Haqdorlik bo'yicha ma'lumot
    """
    auth_organizations = models.ForeignKey(AuthOrganization, on_delete=models.CASCADE, default=1)
    inter_organization = models.ForeignKey(InterOrganization, on_delete=models.CASCADE, default=1)
    initial_amount = models.DecimalField(decimal_places=2, max_digits=12, blank=True)
    final_amount = models.DecimalField(decimal_places=2, max_digits=15, blank=True)
    fiscal_year = models.CharField(default=str(now().year), help_text=_("Fiscal year"), max_length=4)
    created_at = models.DateTimeField(auto_now_add=True)
    deleted = models.BooleanField(default=False)
    deleted_at = models.CharField(default="2021.09.10", blank=True, null=True, max_length=26)

    #  Up to years
    current_1 = models.IntegerField(default=0, blank=True)
    current_2 = models.IntegerField(default=0, blank=True)
    current_3 = models.IntegerField(default=0, blank=True)
    current_4 = models.IntegerField(default=0, blank=True)
    previous_years = models.IntegerField(default=0, blank=True)

    #  Taken account for this year payment
    taken_account = models.IntegerField(default=0, blank=True)


class Birja(models.Model):
    bearer = models.CharField(max_length=255)

    def __str__(self):
        return self.bearer
    
