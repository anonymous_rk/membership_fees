from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _
import os


def number_validator(big_number):
    if not big_number.isnumeric():
        raise ValidationError(_("Iltimos, faqat son kiriting!"))
    return big_number


def phone_nb_validator(phone_number):
    if not phone_number.startswith("+998") and len(phone_number) == 13 or "+" in phone_number and not phone_number[1::].isnumeric():
        raise ValidationError(_("Iltimos, telefon raqamni to'g'ri kiriting. Namuna: +998999999999"))
    if len(phone_number) == 9 and not phone_number.isnumeric():
        raise ValidationError(_("Iltimos, telefon raqamni to'g'ri kiriting. Namuna: +998999999999"))

    return phone_number


def currency_validator(currency):
    if not currency.isalpha():
        raise ValidationError("Iltimos, to'g'ri birlik kiriting. Misol: USD, UZS")

    return currency


def file_validator(file):
    name = str(file)
    # file_format = name.split('.')[-1]
    ext = os.path.splitext(file.name)[-1]
    if not ext.lower() in ['.pdf', '.docx', '.doc']:

        raise ValidationError("File formati quyidagilardan biri bo'lishi lozim: '.PDF', '.DOCX', '.DOC'")
    return file


def account_number_validator(number):
    if not number.isnumeric():
        raise ValidationError("Hisob raqami faqat [0-9] raqamlardan iborat bo'lishi shart.")
    elif len(number) != 9:
        raise ValidationError("Hisob raqami uzunligi 9 ta belgidan iborat bo'lishi shart.")

    return number
