# Generated by Django 3.2.6 on 2021-09-17 11:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0036_auto_20210916_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='payment_date',
            field=models.DateField(default='2021-09-17'),
        ),
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='payment_date_ru',
            field=models.DateField(default='2021-09-17', null=True),
        ),
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='payment_date_uz',
            field=models.DateField(default='2021-09-17', null=True),
        ),
    ]
