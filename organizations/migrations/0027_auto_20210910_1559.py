# Generated by Django 3.2.6 on 2021-09-10 15:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0026_auto_20210909_1642'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='membershipfeescondition',
        #     name='currency',
        # ),
        migrations.AlterField(
            model_name='assets',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='authorganization',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='currencies',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='debt',
            name='created_at',
            field=models.DateTimeField(auto_created=True, blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='debt',
            name='current_1',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='current_2',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='current_3',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='current_4',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='debt',
            name='final_debt',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=15),
        ),
        migrations.AlterField(
            model_name='debt',
            name='initial_debt',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=12),
        ),
        migrations.AlterField(
            model_name='debt',
            name='other_sources',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='paid_from_budget',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='paid_not_from_budget',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='debt',
            name='previous_years',
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AlterField(
            model_name='interorganization',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='linkedauthinterorgs',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-09-10, 15:59', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='payment_date',
            field=models.DateField(default='2021-09-10'),
        ),
    ]
