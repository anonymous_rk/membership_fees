# Generated by Django 3.2.6 on 2021-08-23 17:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0014_auto_20210823_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authorganization',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-23, 17:09', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='currencies',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-23, 17:09', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='interorganization',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-23, 17:09', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='linkedauthinterorgs',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-23, 17:09', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-23, 17:09', max_length=26, null=True),
        ),
    ]
