# Generated by Django 3.2.6 on 2021-08-20 10:16

from django.db import migrations, models
import organizations.validators


class Migration(migrations.Migration):

    dependencies = [
        ('organizations', '0010_auto_20210820_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='membershipfeescondition',
            name='invoice_confirmation',
            field=models.FileField(default='null', help_text='File on confirmation of sent invoce from international organization.', upload_to='confirmations_invoices/', validators=[organizations.validators.file_validator]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membershipfeescondition',
            name='paid_fee_confirmation',
            field=models.FileField(default='null', help_text='File on confirmation of sent invoce from international organization.', upload_to='confirmation_payments/', validators=[organizations.validators.file_validator]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='authorganization',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-20, 10:16', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='currencies',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-20, 10:16', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='interorganization',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-20, 10:16', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='linkedauthinterorgs',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-20, 10:16', max_length=26, null=True),
        ),
        migrations.AlterField(
            model_name='membershipfeescondition',
            name='deleted_at',
            field=models.CharField(blank=True, default='2021-08-20, 10:16', max_length=26, null=True),
        ),
    ]
