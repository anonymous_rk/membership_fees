/**
 * Created by a_djuraev on 22.07.2016.
 * @version: 3.0
 * @author: A Juraev
 * !The variables: version_error, not_found_error must be set
 */


var keys = [];
var app_version = 3.27;
var input = document.getElementById("edc-face");
var dropdown = document.getElementById("edc-dropdown");
var edc_value = document.getElementById("edc_value");
var signed_data = document.getElementById("signed_data");
var not_signed_data = document.getElementById("not_signed_data");
var sendbutton = document.getElementById("service-form-send-button");
var error_text = document.getElementById("alert_text").innerText;
var form_signed = false;
if (sendbutton !== undefined && sendbutton !== null)
    sendbutton.setAttribute('disabled', 'disabled');

function error(e) {
    alert(error_text);
}

function getParent(node, name) {
    node = node.parentNode;
    if (node != undefined && node.nodeName == name.toUpperCase())
        return node;
    if (node != undefined)
        return getParent(node, name);
}

function start_dc_load() {
    CAPIWS.version(function (event, data) {
        var clientV = parseInt(data.major) * 100 + parseInt(data.minor);
        if (!data.success || (clientV < app_version)) {
            show_error(version_error);
        } else {
            CAPIWS.apikey(API_key, function (event, data) {
                if (data.success) {
                    loadKeys();
                } else {
                    alert(data.reason);
                }
            }, error);
        }
    }, error);
}

function getX500Val(s, f) {
    var res = s.split(",");
    for (var i in res) {
        var n = res[i].search(f + "=");
        if (n !== -1)
            return res[i].slice(f.length + 1);
    }
    return "";
}

function setKey(key) {
    var validDate = new Date(key.validTo).getTime() >= Date.now();

    var item = '<div class="itm {invalid-key}">' + item_template + '</div>';
    var node = document.createElement("div");
    node.setAttribute('data-input-val', key.serialNumber);
    node.setAttribute('data-val', key.TIN + " - " + key.CN);
    if (validDate) {
        node.setAttribute('onclick', 'chooseKey(this)');
        item = item.replace('{invalid-date}', '');
    } else {
        item = item.replace('{invalid-date}', 'invalid-date');
        item = item.replace('{invalid-key}', 'invalid-key');
    }


    item = item.replace('{sertificate}', key.serialNumber);
    item = item.replace('{inn}', key.TIN);
    item = item.replace('{fio}', key.CN);
    item = item.replace('{work_place}', key.O);
    item = item.replace('{avaliable_date}', DateFormat(key.validFrom) + " - " + DateFormat(key.validTo));

    if (key.TIN < 400000000)
        item = item.replace('{type}', edc_types['juridical']);
    else
        item = item.replace('{type}', edc_types['physical']);

    var parser = new DOMParser();
    var doc = parser.parseFromString(item, "text/html");
    node.appendChild(doc.getElementsByClassName('itm')[0]);
    dropdown.appendChild(node);
}

function setKeys() {
    while (dropdown.firstChild)
        dropdown.removeChild(dropdown.firstChild);
    for (var key in keys)
        setKey(keys[key]);
}


function listPfxCertificates(disc, callback) {
    CAPIWS.callFunction({plugin: "pfx", name: "list_certificates", arguments: [disc]}, function (event, data) {
        if (data.success) {
            var i = data.certificates.length;
            for (var rec in data.certificates) {
                i--;
                var el = data.certificates[rec];
                var x500name_ex = el.alias.toUpperCase();
                x500name_ex = x500name_ex.replace("1.2.860.3.16.1.1=", "INN=");
                x500name_ex = x500name_ex.replace("1.2.860.3.16.1.2=", "PINFL=");
                var vo = {
                    disk: el.disk,
                    path: el.path,
                    name: el.name,
                    alias: el.alias,
                    serialNumber: getX500Val(x500name_ex, "SERIALNUMBER"),
                    validFrom: new Date(getX500Val(x500name_ex, "VALIDFROM").replace(/\./g, "-")),
                    validTo: new Date(getX500Val(x500name_ex, "VALIDTO").replace(/\./g, "-")),
                    CN: getX500Val(x500name_ex, "CN"),
                    TIN: (getX500Val(x500name_ex, "INN") ? getX500Val(x500name_ex, "INN") : getX500Val(x500name_ex, "UID")),
                    UID: getX500Val(x500name_ex, "UID"),
                    O: getX500Val(x500name_ex, "O"),
                    T: getX500Val(x500name_ex, "T"),
                    type: 'pfx'
                };
                if (!vo.TIN) continue;
                keys[vo.serialNumber] = vo;
                if (i <= 0)
                    callback();
            }
        } else {
            console.log("alert1 :" + data.reason);
        }
    }, function (e) {
        console.log("alert2 :" + e);
    });
};

function listCertKeyCertificates(disc, callback) {
    CAPIWS.callFunction({plugin: "certkey", name: "list_certificates", arguments: [disc]}, function (event, data) {
        if (data.success) {
            var i = data.certificates.length;
            for (var rec in data.certificates) {
                i--;
                var el = data.certificates[rec];
                var vo = {
                    disk: el.disk,
                    path: el.path,
                    name: el.name,
                    serialNumber: el.serialNumber,
                    subjectName: el.subjectName,
                    validFrom: new Date(el.validFrom),
                    validTo: new Date(el.validTo),
                    issuerName: el.issuerName,
                    publicKeyAlgName: el.publicKeyAlgName,
                    CN: getX500Val(el.subjectName, "CN"),
                    TIN: getX500Val(el.subjectName, "INITIALS"),
                    O: getX500Val(el.subjectName, "O"),
                    T: getX500Val(el.subjectName, "T"),
                    type: 'certkey'
                };
                if (!vo.TIN) continue;
                keys[vo.serialNumber] = vo;
                if (i <= 0)
                    callback();
            }
        } else {
            console.log("alert3 :" + data.reason);
        }
    }, function (e) {
        console.log("alert4 :" + e);
    });
};

function listJavaTokenCertificates(disc, callback) {
    CAPIWS.callFunction({plugin: "ftjc", name: "list_all_keys", arguments: ['']}, function (event, data) {
        if (data.success) {
            var i = data.tokens.length;
            for (var rec in data.tokens) {
                i--;
                var el = data.tokens[rec];
                var x500name_ex = el.info.toUpperCase();
                x500name_ex = x500name_ex.replace("1.2.860.3.16.1.1=", "INN=");
                x500name_ex = x500name_ex.replace("1.2.860.3.16.1.2=", "PINFL=");
                var vo = {
                    cardUID: el.cardUID,
                    statusInfo: el.statusInfo,
                    ownerName: el.ownerName,
                    info: el.info,
                    serialNumber: getX500Val(x500name_ex, "SERIALNUMBER"),
                    validFrom: new Date(getX500Val(x500name_ex, "VALIDFROM")),
                    validTo: new Date(getX500Val(x500name_ex, "VALIDTO")),
                    CN: getX500Val(x500name_ex, "CN"),
                    TIN: (getX500Val(x500name_ex, "INN") ? getX500Val(x500name_ex, "INN") : getX500Val(x500name_ex, "UID")),
                    UID: getX500Val(x500name_ex, "UID"),
                    O: getX500Val(x500name_ex, "O"),
                    T: getX500Val(x500name_ex, "T"),
                    type: 'ftjc'
                };
                if (!vo.TIN) continue;
                keys[vo.serialNumber] = vo;
                if (i <= 0)
                    callback();
            }
        } else {
            console.log("alert5 :" + data.reason);
        }
    }, function (e) {
        console.log("alert6 :" + e);
    });
};


function loadKeys() {
    CAPIWS.callFunction({plugin: "pfx", name: "list_disks"}, function (event, data) {
        if (data.success) {
            for (var disc in data.disks) {
                listPfxCertificates(data.disks[disc], function () {
                    setKeys();
                });
            }
        } else {
            console.log("alert5 :" + data.reason);
        }
    }, function (e) {
        console.log("alert6 :" + e);
    });

    CAPIWS.callFunction({plugin: "certkey", name: "list_disks"}, function (event, data) {
        if (data.success) {
            for (var rec in data.disks) {
                listCertKeyCertificates(data.disks[rec], function () {
                    setKeys();
                });
            }
        } else {
            console.log("alert7 :" + data.reason);
        }
    }, function (e) {
        console.log("alert8 :" + e);
    });

    CAPIWS.callFunction({plugin: "ftjc", name: "list_tokens"}, function (event, data) {
        if (data.success) {
            for (var rec in data.tokens) {
                listJavaTokenCertificates(data.tokens[rec], function () {
                    setKeys();
                });
            }
        } else {
            console.log("alert7 :" + data.reason);
        }
    }, function (e) {
        console.log("alert8 :" + e);
    });

};

function DateFormat(date) {
    day = parseInt(date.getDate()) > 9 ? date.getDate() : "0" + date.getDate();
    month = parseInt(date.getMonth()) > 9 ? date.getMonth() : "0" + date.getMonth();
    return day + "." + month + "." + date.getUTCFullYear();
};

function getKey() {
    if (edc_value.value in keys) {
        return keys[edc_value.value];
    }
}

function getSignData() {
    var _jsonMap = tryLoadFromJsonRule();
    console.log(_jsonMap);
    if (_jsonMap != null)
        return _jsonMap;

    if (input_id != null && input_id.length > 0) {
        var e = document.getElementById(input_id);
        if (e.value != undefined && e.value.length > 0)
            return e.value;
    }
    return serialize(getParent(input, 'form'));
}

function tryLoadFromJsonRule() {
    if (json_map != null) {
        var data = json_map;
        $.each(json_map, function (key, fieldCode) {
            data[key] = $('#' + fieldCode).val();
        });

        return JSON.stringify(data);
    }

    return null;
}

function report_success() {
    var el = document.getElementById("signed_s");
    el.style.display = "block";
    el.style.opacity = 0;
    var last = +new Date();
    var tick = function () {
        el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
        last = +new Date();
        if (+el.style.opacity < 1) {
            (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
        }
    };
    tick();
    if (sendbutton)
        sendbutton.removeAttribute('disabled');
}

function postLoadKey(id, vo) {
    CAPIWS.callFunction({
        plugin: "pkcs7",
        name: "create_pkcs7",
        arguments: [Base64.encode(getSignData()), id, 'no']
    }, function (event, data) {
        if (data.success) {
            console.log(id);
            signed_data.value = data.pkcs7_64;
            not_signed_data.value = Base64.encode(getSignData());
            report_success();
        } else {
            console.log("alert9 :" + data.reason);
            if (vo) {
                sessionStorage.removeItem(vo.serialNumber);
                beginSign();
            }
        }
    }, function (e) {
        console.log("alert10 :" + e);
        if (vo) {
            sessionStorage.removeItem(vo.serialNumber);
            beginSign();
        }
    });
};

function beginSign() {
    var vo = getKey();
    if (vo) {
        if (vo.type === "certkey") {
            CAPIWS.callFunction({
                plugin: "certkey",
                name: "load_key",
                arguments: [vo.disk, vo.path, vo.name, vo.serialNumber]
            }, function (event, data) {
                if (data.success) {
                    sessionStorage.setItem(vo.serialNumber, data.keyId);
                    postLoadKey(data.keyId);
                } else {
                    console.log("alert11 :" + data.reason);
                }
            }, function (e) {
                console.log("alert12 :" + e);
            });
        } else if (vo.type === "pfx") {
            CAPIWS.callFunction({
                plugin: "pfx",
                name: "load_key",
                arguments: [vo.disk, vo.path, vo.name, vo.alias]
            }, function (event, data) {
                if (data.success) {
                    sessionStorage.setItem(vo.serialNumber, data.keyId);
                    postLoadKey(data.keyId);
                } else {
                    console.log("alert13 :" + data.reason);
                }
            }, function (e) {
                console.log("alert14 :" + e);
            });
        } else if (vo.type === "ftjc") {
            CAPIWS.callFunction({plugin: "ftjc", name: "load_key", arguments: [vo.cardUID]}, function (event, data) {
                if (data.success) {
                    sessionStorage.setItem(vo.serialNumber, data.keyId);
                    postLoadKey(data.keyId);
                } else {
                    console.log("alert15 :" + data.reason);
                }
            }, function (e) {
                console.log("alert16 :" + e);
            });
        }
    }
};

function chooseKey(key) {
    edc_value.value = key.getAttribute('data-input-val');
    input.value = key.getAttribute('data-val');
}

window.onload = function () {
    // sendbutton.onclick = function (e) {
    //     if(!form_signed)
    //         e.defaultPrevented = true;
    //     else
    //         e.defaultPrevented = false;
    // };

    input.onkeydown = function (e) {
        e.defaultPrevented = true;
        return false;
    };

    input.onclick = function (e) {
        start_dc_load();
        $("#edcModal").modal("show");
        dropdown.setAttribute('style', 'display: block;');
    };

    document.getElementById("sign").onclick = function () {
        var vo = getKey();
        $("#edcModal").modal("hide");
        if (sessionStorage.getItem(vo.serialNumber))
            postLoadKey(sessionStorage.getItem(vo.serialNumber), vo)
        else
            beginSign();
    };

    document.onclick = function (e) {
        if (e.target.getAttribute('id') != 'edc-face')
            $("#edcModal").modal("hide");
        // dropdown.setAttribute('style','display: none;');
    };

    // Very mportant do not touch
    do {
        'nothing'
    } while (false);

}


//////////////////////////////////////////////////////////////
function serialize(form) {
    if (!form || form.nodeName !== "FORM") {
        return;
    }
    var i, j, q = [];
    for (i = form.elements.length - 1; i >= 0; i = i - 1) {
        if (form.elements[i].name === "") {
            continue;
        }
        switch (form.elements[i].nodeName) {
            case 'INPUT':
                switch (form.elements[i].type) {
                    case 'text':
                    case 'hidden':
                    case 'password':
                    case 'button':
                    case 'reset':
                    case 'submit':
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        break;
                    case 'checkbox':
                    case 'radio':
                        if (form.elements[i].checked) {
                            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        }
                        break;
                    case 'file':
                        break;
                }
                break;
            case 'TEXTAREA':
                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                break;
            case 'SELECT':
                switch (form.elements[i].type) {
                    case 'select-one':
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        break;
                    case 'select-multiple':
                        for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                            if (form.elements[i].options[j].selected) {
                                q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
                            }
                        }
                        break;
                }
                break;
            case 'BUTTON':
                switch (form.elements[i].type) {
                    case 'reset':
                    case 'submit':
                    case 'button':
                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                        break;
                }
                break;
        }
    }
    return q.join("&");
}