var EIMZO_MAJOR = 3;
var EIMZO_MINOR = 37;


var errorCAPIWS = 'Ошибка соединения с E-IMZO. Возможно у вас не установлен модуль E-IMZO или Браузер E-IMZO.';
var errorBrowserWS = 'Браузер не поддерживает технологию WebSocket. Установите последнюю версию браузера.';
var errorUpdateApp = 'ВНИМАНИЕ !!! Установите новую версию приложения E-IMZO или Браузера E-IMZO.<br /><a href="https://e-imzo.uz/main/downloads/" role="button">Скачать ПО E-IMZO</a>';
var errorWrongPassword = 'Пароль неверный.';


var AppLoad = function () {
    EIMZOClient.API_KEYS = [
        'localhost', '96D0C1491615C82B9A54D9989779DF825B690748224C2B04F500F370D51827CE2644D8D4A82C18184D73AB8530BB8ED537269603F61DB0D03D2104ABF789970B',
        // 'localhost', '6C1414D752583EF343292D72F2CB8538CDE35B1A8C5B86775BB523DA654ECD8BE2EB703EEAA4E790A6F4791126A3DE7A3FC3D77D541085E38B032019C2DC26A6СТИР',
        'badal-tolovlari.mf.uz', '6C1414D752583EF343292D72F2CB8538CDE35B1A8C5B86775BB523DA654ECD8BE2EB703EEAA4E790A6F4791126A3DE7A3FC3D77D541085E38B032019C2DC26A6',
        // 'null', 'E0A205EC4E7B78BBB56AFF83A733A1BB9FD39D562E67978CC5E7D73B0951DB1954595A20672A63332535E13CC6EC1E1FC8857BB09E0855D7E76E411B6FA16E9D',
        //'dls.yt.uz', 'EDC1D4AB5B02066FB3FEB9382DE6A7F8CBD095E46474B07568BC44C8DAE27B3893E75B79280EA82A38AD42D10EA0D600E6CE7E89D1629221E4363E2D78650516'
    ];

    uiLoading();
    EIMZOClient.checkVersion(function (major, minor) {
        var newVersion = EIMZO_MAJOR * 100 + EIMZO_MINOR;
        var installedVersion = parseInt(major) * 100 + parseInt(minor);
        if (installedVersion < newVersion) {
            uiUpdateApp();
        } else {
            EIMZOClient.installApiKeys(function () {
                uiLoadKeys();
            }, function (e, r) {
                if (r) {
                    uiShowMessage(r);
                } else {
                    wsError(e);
                }
            });
        }
    }, function (e, r) {
        if (r) {
            uiShowMessage(r);
        } else {
            uiNotLoaded(e);
        }
    });
}


var uiShowMessage = function (message) {
    alert(message);
}

var uiLoading = function () {
    var l = document.getElementById('message');
    l.innerHTML = 'Загрузка ...';
    l.style.color = 'red';
}

var uiNotLoaded = function (e) {
    var l = document.getElementById('message');
    l.innerHTML = '';
    if (e) {
        wsError(e);
    } else {
        uiShowMessage(errorBrowserWS);
    }
}

var uiUpdateApp = function () {
    var l = document.getElementById('message');
    l.innerHTML = errorUpdateApp;
}

var getDate = function (date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [day, month, year].join('.');
}
var cardItem = function (itemId, item) {

    let user = (item.O != undefined)? `${item.O}<br>` : '';
    let expired_class = (item.expired != undefined && item.expired == true)? 'expired' : '';

    return `
        <div data-tin="${item.TIN}" class="item ${expired_class} hidden" vo="${JSON.stringify(item)}" id="${itemId}">
            <strong class="user">${user}</strong><br>
            <strong class="keyItemRow" > ЭРИ амал қилиш муддати: ${getDate(item.validFrom)} - ${getDate(item.validTo)} </strong>
            <br>
            <strong class="full_name">Ф.И.Ш.: ${item.CN}</strong>
            <div>
                СТИР: ${item.TIN}
            </div>
            
        </div>
    `;
};

function assign_selections() {
    let selected_option = $("#user option:selected");
        if (selected_option){
            let selected_item = JSON.parse(selected_option.attr('vo'));

            $.each($("#keys .item"), function (index, item) {
                let item_tin = $(item).data('tin');
                if (selected_item.TIN == item_tin){
                    $(item).removeClass('hidden').addClass('open selected');
                }else{
                    $(item).addClass('hidden').removeClass('open').removeClass('selected')
                }
            })
        }
}

var uiLoadKeys = function () {
    uiClearCombo();
    let keys = document.getElementById('keys');
    EIMZOClient.listAllUserKeys(function (o, i) {
        var itemId = "itm-" + o.serialNumber + "-" + i;
        return itemId;
    }, function (itemId, v) {
        var now = new Date();
        v.expired = dates.compare(now, v.validTo) > 0;

        keys.innerHTML += cardItem(itemId, v);
        return uiCreateItem(itemId, v);
    }, function (items, firstId) {
        uiFillCombo(items);
        uiLoaded();
        uiComboSelect(firstId);
        assign_selections();

    }, function (e, r) {
        uiShowMessage(errorCAPIWS);
    });
}

var uiComboSelect = function (itm) {
    if (itm) {
        var id = document.getElementById(itm);
        id.setAttribute('selected', 'true');
    }
}

var cbChanged = function (c) {
    document.getElementById('keyId').innerHTML = '';
    assign_selections();
}

var uiClearCombo = function () {
    var combo = document.testform.key;
    combo.length = 0;
}

var uiFillCombo = function (items) {
    var combo = document.testform.key;
    for (var itm in items) {
        combo.append(items[itm]);
    }
}

var uiLoaded = function () {
    var l = document.getElementById('message');
    l.innerHTML = '';
}

var uiCreateItem = function (itmkey, vo) {
    var now = new Date();
    vo.expired = dates.compare(now, vo.validTo) > 0;
    var itm = document.createElement("option");
    itm.value = itmkey;
    itm.text = vo.CN;


    if (!vo.expired) {

    } else {
        itm.style.color = 'gray';
        itm.text = itm.text + ' (срок истек)';
        itm.setAttribute('disabled', 'disabled');
    }
    itm.setAttribute('vo', JSON.stringify(vo));
    itm.setAttribute('id', itmkey);
    return itm;
}

var wsError = function (e) {
    if (e) {
        uiShowMessage(errorCAPIWS + " : " + e);
    } else {
        uiShowMessage(errorBrowserWS);
    }
};
auth = function(vo, pkcs7)
{
    let auth_user = {
        tin: vo.TIN,
        pinfl: vo.PINFL,
        serialNumber: vo.serialNumber,
        pkcs7: pkcs7,
        full_name: vo.CN,
        valid_to: `${ getDate(vo.validTo) }`

    };
    $.post('/imzo/', auth_user)
        .done((response) => {
            console.log(response)
            location.href = '/uz/main/'
            console.log('success')
        }).fail((fail_response) => {
            console.log(fail_response)
            console.error('Login failed')
        })
    // $.ajax(
    //     {
    //         type: "POST",
    //         contentType: "application/json",
    //         url: "/uz/imzo/",
    //         data: {
    //             inn: vo.TIN
    //         },
    //         success: {
    //             url: "/uz/main/"
    //         }
    //     }
    // )
}
sign = function () {
    var itm = document.querySelector('.item.selected').getAttribute('id');
    if (itm) {
        var id = document.getElementById(itm);
        var vo = JSON.parse(id.getAttribute('vo'));
        var data = '123';
        var keyId = document.getElementById('keyId').innerHTML;
        if (keyId) {
            EIMZOClient.createPkcs7(keyId, data, null, function (pkcs7) {
                // Success
                document.testform.pkcs7.value = pkcs7;

                auth(vo, pkcs7, )
            }, function (e, r) {
                if (r) {
                    if (r.indexOf("BadPaddingException") != -1) {
                        uiShowMessage(errorWrongPassword);
                    } else {
                        uiShowMessage(r);
                    }
                } else {
                    document.getElementById('keyId').innerHTML = '';
                    uiShowMessage(errorBrowserWS);
                }
                if (e) wsError(e);
            });
        } else {
            EIMZOClient.loadKey(vo, function (id) {
                document.getElementById('keyId').innerHTML = id;
                EIMZOClient.createPkcs7(id, data, null, function (pkcs7) {
                    // Success
                    document.testform.pkcs7.value = pkcs7;
                    // Check INN
                    console.log(vo.TIN);
                    // $.ajax(
                    //     {
                    //         type: "POST",
                    //         contentType: "application/json",
                    //         url: "/imzo/",
                    //         data: {
                    //             inn: vo.TIN
                    //         },
                    //         success: {
                    //             url: "/uz/main/"
                    //         }
                    //     }
                    // )
                    // window.location.replace('/uz/main/');
                auth(vo, pkcs7)
                }, function (e, r) {
                    if (r) {
                        if (r.indexOf("BadPaddingException") != -1) {
                            uiShowMessage(errorWrongPassword);
                        } else {
                            uiShowMessage(r);
                        }
                    } else {
                        document.getElementById('keyId').innerHTML = '';
                        uiShowMessage(errorBrowserWS);
                    }
                    if (e) wsError(e);
                });
            }, function (e, r) {
                if (r) {
                    if (r.indexOf("BadPaddingException") != -1) {
                        uiShowMessage(errorWrongPassword);
                    } else {
                        uiShowMessage(r);
                    }
                } else {
                    uiShowMessage(errorBrowserWS);
                }
                if (e) wsError(e);
            });
        }
    }
};
signUp = function(vo, pkcs7)
{
    let user = {
        tin: vo.TIN,
        serialNumber: vo.serialNumber,
        pkcs7: pkcs7,
        full_name: $('#staff_full_name').val(),
        position: $('#staff_position').val(),
    };
    $.post('/v1/tin-register/', user)
        .done((response) => {
            location.href = '/login'
        }).fail((fail_response) => {
            console.error('Register failed')
        })
}
register = function (e) {
    e.preventDefault();
    console.log('logging')
    var itm = document.testform.key.value;
    if (itm) {
        var id = document.getElementById(itm);
        var vo = JSON.parse(id.getAttribute('vo'));
        var data = '123';
        var keyId = document.getElementById('keyId').innerHTML;
        if (keyId) {
            EIMZOClient.createPkcs7(keyId, data, null, function (pkcs7) {
                // Success
                document.testform.pkcs7.value = pkcs7;

                signUp(vo, pkcs7, )
            }, function (e, r) {
                if (r) {
                    if (r.indexOf("BadPaddingException") != -1) {
                        uiShowMessage(errorWrongPassword);
                    } else {
                        uiShowMessage(r);
                    }
                } else {
                    document.getElementById('keyId').innerHTML = '';
                    uiShowMessage(errorBrowserWS);
                }
                if (e) wsError(e);
            });
        } else {
            EIMZOClient.loadKey(vo, function (id) {
                document.getElementById('keyId').innerHTML = id;
                EIMZOClient.createPkcs7(id, data, null, function (pkcs7) {
                    // Success
                    document.testform.pkcs7.value = pkcs7;
                    signUp(vo, pkcs7)
                }, function (e, r) {
                    if (r) {
                        if (r.indexOf("BadPaddingException") != -1) {
                            uiShowMessage(errorWrongPassword);
                        } else {
                            uiShowMessage(r);
                        }
                    } else {
                        document.getElementById('keyId').innerHTML = '';
                        uiShowMessage(errorBrowserWS);
                    }
                    if (e) wsError(e);
                });
            }, function (e, r) {
                if (r) {
                    if (r.indexOf("BadPaddingException") != -1) {
                        uiShowMessage(errorWrongPassword);
                    } else {
                        uiShowMessage(r);
                    }
                } else {
                    uiShowMessage(errorBrowserWS);
                }
                if (e) wsError(e);
            });
        }
    }
};

window.onload = AppLoad;
